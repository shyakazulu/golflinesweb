Rails.application.routes.draw do

  mount Ckeditor::Engine => '/ckeditor'
  namespace :admin do
    resources :stock_level_adjustments
    resources :products
    resources :orders do
      member do
        get :ship
        get :accept
        get :reject
      end
    end
    resources :articles do
      member {post :like}
    end
    resources :promotions
    resources :tax_rates
    resources :users
    resources :countries
    resources :addresses
    resources :currencies
    resources :product_categories
    resources :payments, only: [:new, :create] do
      member do
        get :confirm
      end
    end
    resources :delivery_services do
      resources :delivery_service_prices
    end
  end
  resources :comments do
    member {post :like}
  end

  get '/admin', to: "admin/products#index"

  scope "(:locale)",locale: /#{I18n.available_locales.join("|")}/ do
  
    resources :user_sessions, only: [:new, :create]
    resources :password_resets
    get '/store/:permalink', to: "public#show", as: :item
    get '/home', to: "public#index", as: :home
    get '/store', to: "public#store", as: :store
    get "/login" => "user_sessions#new", as: :login
    get "/register" => "admin/users#register", as: :register
    get "/logout" => "user_sessions#destroy", as: :logout
    get 'user_sessions/create'
    get 'user_sessions/destroy'
    get '/cart', to: "cart#index", as: :cart
    get '/add', to: "cart#add", as: :add
    get '/clear', to: "cart#clear", as: :clear
    get '/remove', to: "cart#remove", as: :remove
    get '/set-address/(:delivery)/(:token)', to: "admin/addresses#new", as: :set_address
    get '/set-delivery/(:token)', to: "process#delivery", as: :set_delivery
    get '/blog', to: "blog#blog", as: :blog
    get '/blog/:permalink', to: "blog#article", as: :blog_article
    get '/about', to: "public#about", as: :about
    get '/contact', to: "public#contact", as: :contact
    get '/payment/:token', to: "admin/payments#new", as: :purchase
    get '/terms', to: "public#terms", as: :terms
    root 'public#index'
  end
  match '', to: redirect("/#{I18n.default_locale}/home"), :via => [:get]
  match 'admin/:controller(/:action(/:id))', :via => [:get, :post, :patch]
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
