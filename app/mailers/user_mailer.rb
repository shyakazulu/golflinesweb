class UserMailer < ApplicationMailer

  default from: "no-reply@golflinesofficial.com"

  def password_reset(user)
  	@user = user
  	mail to: @user.email, subject: "Password Reset"
  end

  def welcome(user)
  	@user = user
  	mail to: @user.email, subject: "Welcome to GolfLines official"
  end
end
