
/*
  Public Area javascript can be found here
  except for payment related validations which you
  can find in the validations.js file in the same directory
*/

var ready = function() {

    // Body fade in animation
    setTimeout(showBody, 1000);
    function showBody (argument) {
       $('.loader').addClass('hide');
       $('.site').addClass('show');
    }

    setTimeout(showAuth, 2000);
    function showAuth (argument) {
       $('.timefade').addClass('show');
    }

   // main nav scroll effect
   var headerHeight = $('.header').height();
   $(window).on('scroll',
      {
         previousTop: 0
      },
      function() {
         var currentTop = $(window).scrollTop();
         //check if user is scrolling up
         if (currentTop < this.previousTop) {
         	// if scrolling up..
         	if (currentTop > 0 && $('.header').hasClass('is-fixed')){
              $('.header').addClass('is-visible');
              $('#golflines-btm').removeClass('visible');
         	} else {
         	  $('.header').removeClass('is-visible is-fixed');
             $('#golflines-btm').addClass('visible');
         	}
         } else {
            //if scrolling down
            $('.header').removeClass('is-visible');
            if( currentTop > headerHeight && !$('.header').hasClass('is-fixed')) $('.header').addClass('is-fixed');
         }
         this.previousTop = currentTop;
         //Toggle scroller opacity
         if(currentTop > 420) {
            $('.scroller').addClass('fade');
         } else{
            $('.scroller').removeClass('fade');
         }
    });
   //show mav menu
   $('.primary-nav-trigger').click(function(event) {
     $('.menu-icon').toggleClass('on');
     $('.menu-fixed').toggleClass('shown');
   });
   //close modal when clicking the esc keyboard button
   $(document).keyup(function(event){
      if(event.which=='27'){
        $('.menu-icon').removeClass('on');
        $('.menu-fixed').removeClass('shown');
      }
   });
   // Store navigation activity
   $('ul.shop-category li').on('click', 'a',function(event) {
      $('ul.shop-category li a').removeClass('nav-on').removeClass('expand');
      $(this).addClass('expand').addClass('nav-on');
   });

   // Smooth scrolling

   $('#top').click(function() {
     $("html, body").animate({ scrollTop: 0 }, "slow");
     return false;
   });

   $('.explore').click(function() {
     var distance = $("section.quote").offset().top;
     $("html, body").animate({ scrollTop: distance }, "slow");
     return false;
   });
   //cart animation
   $('.cart-trigger').click(function() {
      if ($('.cart').hasClass('visible-cart')) {
        $('.cart').addClass('hidden-cart').removeClass('visible-cart');
      } else {
         $('.cart').addClass('visible-cart').removeClass('hidden-cart');
      }
   });
  // Slider 1

  var slidesWrapper = $('.slider-1'),
      slidesWrapperSm = $('.slider-2'),
      slidesNumber = slidesWrapper.children('li').length,
      visibleSlidePosition = 0,
      autoPlayId,
      autoPlayDelay = 5000,
      slideNav = $('.slider-control');

  //autoplay slider
  setAutoplay(slidesWrapper, slidesNumber, autoPlayDelay);
  // setAutoplay(slidesWrapperSm, slidesNumber, autoPlayDelay);

  slideNav.on('click', 'li', function(event) {
     event.preventDefault();
     var selectedItem = $(this);
     if(!selectedItem.hasClass('selected')) {
        var selectedPosition = selectedItem.index(),
            activePosition = slidesWrapper.find('li.selected').index();

        if(activePosition < selectedPosition) {
          nextSlide(slidesWrapper.find('.selected'), slidesWrapper, slideNav, selectedPosition);
          nextSlide(slidesWrapperSm.find('.selected'), slidesWrapperSm, slideNav, selectedPosition);
        } else {
          prevSlide(slidesWrapper.find('.selected'), slidesWrapper, slideNav, selectedPosition);
          prevSlide(slidesWrapperSm.find('.selected'), slidesWrapperSm, slideNav, selectedPosition)
        }

        visibleSlidePosition = selectedPosition;

        updateSliderNavigation(slideNav, selectedPosition);
        //reset autoplay
        setAutoplay(slidesWrapper, slidesNumber, autoPlayDelay);
        // setAutoplay(slidesWrapperSm, slidesNumber, autoPlayDelay);
     }
  });

  function nextSlide(visibleSlide, container, pagination, n) {
      visibleSlide.removeClass('selected').addClass('is-moving').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
        visibleSlide.removeClass('is-moving');
      });
      container.children('li').eq(n).addClass('selected').prevAll().addClass('move-left');
  }

  function prevSlide (visibleSlide, container, pagination, n) {
      visibleSlide.removeClass('selected').addClass('is-moving').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
        visibleSlide.removeClass('is-moving');
      });
      container.children('li').eq(n).addClass('selected').removeClass('move-left').nextAll().removeClass('move-left');
  }

  function updateSliderNavigation(pagination, n) {
     var navigationDot = pagination.find('.selected');
     navigationDot.removeClass('selected');
     pagination.find('li').eq(n).addClass('selected');
  }

  function setAutoplay(wrapper, length, delay) {
    if(wrapper.hasClass('autoplay')) {
      clearInterval(autoPlayId);
      autoPlayId = window.setInterval(function(){autoplaySlider(length, wrapper)}, delay);
    }
  }

  function autoplaySlider(length, wrapper) {
    if( visibleSlidePosition < length - 1) {
      nextSlide(wrapper.find('.selected'), wrapper, slideNav, visibleSlidePosition + 1);
      visibleSlidePosition +=1;
    } else {
      prevSlide(wrapper.find('.selected'), wrapper, slideNav, 0);
      visibleSlidePosition = 0;
    }

    updateSliderNavigation(slideNav, visibleSlidePosition);
  }

  // Product view images display navigation.
  var wrapperOne = $("#wrap-1"),
      wrapperTwo = $("#wrap-2"),
      wrapperThree = $("#wrap-3"),
      wrapperFour = $("#wrap-4"),
      displayControl = $('.analog');

  function displayProduct(container, control) {
    var displayNumber = container.children('li').length;

    $(control).click(function(event) {
      event.preventDefault();
      var activePosition = container.find('li.visi').index(),
          visi = container.find('li.visi');
      visi.removeClass('visi');
      if (activePosition < displayNumber - 1) {
        container.children('li').eq(activePosition + 1).addClass('visi');
      } else {
        container.children('li').eq(0).addClass('visi');
      }

    });

  }

  displayProduct(wrapperOne, displayControl);
  displayProduct(wrapperTwo, displayControl);
  displayProduct(wrapperThree, displayControl);
  displayProduct(wrapperFour, displayControl);

  // Appending flash message.

  function hideFlash() {
    $('.flash-message').removeClass('show-flash').addClass('hide-flash');
  }

  function removeFlash() {
    $('.flash-message').remove();
  }

  function showFlash() {
    $('.flash-message').addClass('show-flash');
  }

  function displayFlash(type, content) {
    var header = $('#prep');
    removeFlash();
    header.prepend('<div class="flash-message public js">\
      <p id=' + type + '>' + content + '</p>\
      </div>');

    setTimeout(showFlash, 1000);
    setTimeout(hideFlash, 4000);
  }


  // Cart form submition validation.
      submit = $(".commit");
  function validateForm(id,form) {
    var cartForm = document.forms[id],
        size = cartForm.elements["size"].value,
        validSizes = ["s", "m", "l","xl", "xxl"],
        quantity = cartForm.elements["quantity"].value,
        url = "/add";
    $("html, body").animate({ scrollTop: 0 }, "slow");
    if ($.inArray(size, validSizes) !== -1 && quantity > 0) {
      // Submit the form with Ajax.
       $.ajax({
           type: "get",
           url: url,
           data: $('#' + id).serialize(),
           success: function(data)
           {
              // Display success Flash message.
              var re = /\/cn\//;
              if(re.test(window.location.href)){
                displayFlash("success", "你已经增加了这到购物");
              }
              else {
                displayFlash("success", "You've just added this product to your cart.");
              }
           }
         });

    } else {
       // Display alert flash message.
       var re = /\/cn\//;
       if(re.test(window.location.href)){
          displayFlash("alert", "请选择数量和大小");
       }
       else {
          displayFlash("alert", "Please select the right quantity and size.");
       }
    }
  }
  // toggle cart visibility in store
  $('.toggler').click(function(event) {
    $('.show-menu').removeClass('show-menu').addClass('hide-menu').closest('div').toggleClass('to-back');
    var target = $(this).attr('id');
    if ($('#' + target + '_list').hasClass('hide-menu')){
        $('#' + target + '_list').toggleClass('hide-menu').toggleClass('show-menu');
        $('#' + target + '_sizes').toggleClass('to-back');
    }
    
  });

  submit.click(function(event) {
    var id = $(this).closest('form').attr('id'),
        form = $(this).closest('form');
    event.preventDefault();
    validateForm(id,form);
  });

  $('.commit').click(function(event) {
    $(this).closest('ul').toggleClass('hide-menu').toggleClass('show-menu');
    $(this).closest('div').toggleClass('to-back');
    // $(".sizes").toggleClass('to-back');
  });
  // chosen js initialization
  $('.chosen-select').chosen({
      width: '100%'
  });

  // show promotion pop-up

  function showPromo () {
    $('.promo-banner').removeClass('hid').addClass('shown');
  }

  $('#dismiss').click(function(event) {
    $('.promo-banner').removeClass('shown').addClass('hid');
  });

  if (typeof gon !== "undefined" && typeof gon.promo !== "undefined" ){
    setTimeout(showPromo, 10000);
  }


};

$(document).ready(ready);
$(document).on('page:load', ready);
