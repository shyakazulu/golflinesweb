/* 
  Admin area javascript can be found here
*/ 

var ready = function() {

	var sideNav = $('.cd-side-nav');
	var mainView = $('.cd-main-content');

	// Side nav animation
   $(".main").mousedown(function(event) {
   	  $(this).css({
   	  	'background-color': '#373E47'
   	  });
   }).mouseup(function(event) {
   	  $(this).css({
   	  	'background-color': '#2c3139'
   	  });
   });
   // Hide side navigation
   $(".main").click(function(event) {
   	 sideNav.addClass('nav-is-hidden').removeClass('show-nav');
   	 $('.show').removeClass('slide-right').toggleClass('slide-left');

   });
   // Show side navigation
   $("#menu-trigger").click(function(event) {
   	 (sideNav.hasClass('nav-is-hidden')) ? sideNav.addClass('show-nav').removeClass('nav-is-hidden') : console.log('yay');
   	 $('.show').removeClass('slide-left').toggleClass('slide-right');
   });
   // SLA form submission
   $('#add-sla').click(function(event) {
      event.preventDefault();
      $('#sla-form').submit(function(){
         var values = $(this).serialize();
         $$.ajax({
            url: $(this).attr('action'),
            type: "POST",
            dataType: "JSON",
            data: values
         })
         .done(function() {
            console.log("success");
         })
         .fail(function() {
            console.log("error");
         })
         .always(function() {
            console.log("complete");
         });
         
      });
   });
   
   // adding error class 
   $("small").addClass('error');

   // hiding the flash-message div 
   if($('.flash-message').length > 0){
     setTimeout(hideFlash, 6000);  
   }
   function hideFlash(){
      $('.flash-message').addClass('hide-flash');
      $('.cd-header').addClass('hide-header');
   }
   // braintree payment set up
   if (typeof gon !== "undefined" && typeof gon.client_token !== "undefined" ){
      braintree.setup(gon.client_token, "dropin", {
        container: "dropin"
      });
   }

   // show promotion -pop-up
   if (typeof gon !== "undefined" && typeof gon.promo !== "undefined" ){
      $('.promo-banner').removeClass('hid');
   }
};

$(document).ready(ready);
$(document).on('page:load', ready);