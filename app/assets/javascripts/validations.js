/* 
   Payment related validations
*/ 

var validate = function() {
    // Shipping address 
    var shippingForm = $('#new_address'),
        billingForm = $('.bill-address'),
        addressButton = $('.add_commit'),
        check = $('#use_billing_address');
        checkValue = false;

    check.change(
    	function(){
    		if (this.checked) {
                $('.shipping-form').removeClass('display-block').addClass('no-display');
                $('.no-delivery').removeClass('no-display').addClass('display-block');
                checkValue = true;
    		} else {
    			$('.shipping-form').removeClass('no-display').addClass('display-block');
    			$('.no-delivery').removeClass('display-block').addClass('no-display');
    			checkValue = false;
    		}
    	}
    );
    $('#comment_content').val("");
    $('#comment_content').keyup(function(e) {
        var input = this.value;
        if(input.length > 2 && input.length < 4){
           $('#submit-comment').removeClass('disabled').removeClass('opaque').addClass('enable');
        } else if (input.length > 3) {
           $('#submit-comment').addClass('opaque').removeClass('disabled');
        } else {
           $('#submit-comment').addClass('disabled').removeClass('opaque').removeClass('enable');
        }
    });

};

$(document).ready(validate);
$(document).on('page:load', validate);
