module ApplicationHelper

	#side-nav activity
	def nav_active(cname)
        if (cname == controller_name) || (cname == controller_name + "_customers"  && params[:role].present? && params[:role] == "customer")
			"active"
        elsif (cname == controller_name + "_admins" && params[:role].present? && params[:role] == "admin")
            "active"
		end
	end
    #image link
	def link_to_image(image_path, target_link, options={})
		link_to(image_tag(image_path, :border => "0"), target_link, options)
	end
    # stock status font color
	def stock_status(stock)
		'no-stock' unless stock > 0
	end
    # checks for errors on attributes
    def error_on?(object,attribute)
      "error" if object.errors[attribute].size > 0
    end
    # displays image if attachment exists
	def img_tag(attachment)
	  if attachment.file_url.nil?
	    image_tag "cover.svg"
	  else
	    image_tag attachment.file.thumb.url.to_s
	  end
    end
    # displays product image (per size)
    def p_img_tag(attachment, string)
       image_tag(attachment.file.medium.url.to_s, class: string)
    end

    def lg_img_tag(attachment, string)
       image_tag(attachment.file.large.url.to_s, class: string)
    end

    def xl_img_tag(attachment, string)
       image_tag(attachment.file.xlarge.url.to_s, class: string)
    end

    def thumb_img_tag(attachment, string)
        attachment.file.thumb.url.nil? ? image_tag("thumbnail.svg", class: string) : image_tag(attachment.file.thumb.url.to_s, class: string)
    end
    # checks controller and displays appropriate authentication hyperlink
    def auth_link_tag
    	if controller_name == "user_sessions"
          link_to "#{t 'layout.sign_up'}", register_path
        else
           link_to "#{t 'layout.sign_in'}", login_path
    	end
    end
    # displays product image in cart
    def cart_img_tag(attachment, string)
    	image_tag(attachment.file.small.url.to_s, class: string)
    end
    # displays embeded svg
    def embedded_svg filename, options={}
	  file = File.read(Rails.root.join('app', 'assets', 'images', filename))
	  doc = Nokogiri::HTML::DocumentFragment.parse file
	  svg = doc.at_css 'svg'
	  if options[:class].present?
	    svg['class'] = options[:class]
	  end
	  doc.to_html.html_safe
	end
    # displays appropriate price according to the currency parameter
	def price_tag(price)
		if params[:locale].present? 
            locale = params[:locale]
            currency = Currency.find_by_locale(locale)
            cost = currency.rate * price.to_i
            cost.to_i.to_s + " " + currency.name + " " 
        else
            currency = Currency.find_by_locale("en")
            cost = currency.rate * price.to_i
            cost.to_i.to_s + " " + currency.name + " "
		end
	end
    # adds the 'hashed' class to <h2> tag of promoted product price 
	def price_promo(product)
		if product.promoted? && product.promotion.active
			"hashed"
		else
			"no-hash"
		end
	end
    # Hide delivery form
    def hide_form(arg)
        case arg
        when 1
            "no-display" if params[:delivery].present? || flash[:ship].present?
        when 2
            "no-display" if !params[:delivery].present? || flash[:bill].present?
        end
    end
    #  adds the class of nav-on to active category tab
	def nav_on(id)
		if params[:cat].present?
			category = params[:cat].to_i
			if category == id.to_i 
              "nav nav-on"
            else
            	"nav"
			end
		end
	end
	# adds the class no-stock when product stock is insufficient
    def availability(product, size)
    	@product = product
    	case size
    	when 's'
    		adjustment = @product.stock_sm
        when 'm'
    		adjustment = @product.stock_m
    	when 'l'
    		adjustment = @product.stock_lg
    	when 'xl'
    		adjustment = @product.stock_xl
    	when 'xxl'
    		adjustment = @product.stock_xxl
    	when 'any'
    		adjustment = @product.total_stock
    	end
    	"no-stock" if adjustment == 0
    end

    # I18n tag language selection
    def locale_tag
        if params[:locale].present? && params[:locale] == "cn"
          link_to "English", locale: "en"
        else
          link_to "中文", locale: "cn" 
        end
    end

    def admin_locale_tag
        if params[:locale].present? && params[:locale] == "cn"
          link_to locale: "en" do
            button_tag "English", class: 'button small round dark locale-btn'
          end
        else
          link_to locale: "cn" do
            button_tag "中文", class: 'button small round dark locale-btn'
          end
        end
    end

    # I18n locale explore
    def explore_tag
        if params[:locale].present? && params[:locale] == "cn"
          'explore-cn.svg'
        else
          'explore.svg'
        end
    end
    # delivery hyperlink
    def delivery_path
        if params[:token].present?
            set_delivery_path(params[:token])
        else
            set_delivery_path()
        end
    end
    # returns order token if present in params
    def set_token
        params[:token].present? ? params[:token] : nil
    end

    def status_tag(boolean, options={})
        options[:true_text] ||= " "
        options[:false_text] ||= " "

        if boolean
          content_tag(:i, options[:true_text], class: "icon-true")
        else
          content_tag(:i, options[:true_text], class: "icon-false") 
        end
    end

end
