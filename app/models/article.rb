class Article < ActiveRecord::Base
	#relationships
	belongs_to :admin, class_name: "User"
	has_many :comments
    has_many :attachments, :as => :parent, :dependent => :destroy, :autosave => true
    has_many :evaluations, class_name: "RSEvaluation", as: :target

    has_reputation :likes, source: :user, aggregated_by: :sum

    #validations
    validates :title, presence: true
    validates :content, presence: true
    validates :permalink, presence: true ,
                          uniqueness: true
    # validates :admin, presence: true
    validates :pub_date, presence: true

    # globalize gem initialization
    translates :title, :content

    # Nested attributes for attachments
    accepts_nested_attributes_for :attachments, allow_destroy: true 

    # Before validation, set the permalink if we don't already have one
    before_validation { self.permalink = self.title.parameterize if self.permalink.blank? && self.title.is_a?(String) }
    
    #All published articles
    scope :published, -> {where(visible: true)}
    scope :ordered, -> {order(:pub_date => :desc)}
    
    def has_attachments?
      self.attachments.count > 0 
    end

    #determines wether user has liked an item already
    def was_liked?(item)
      evaluations.where(source_type: item.class, source_id: item.id).present?
    end
    
end
