class Address < ActiveRecord::Base

	belongs_to :customer, class_name: "User"
	has_many :billed_orders, foreign_key: "billing_address_id"
	has_many :shipped_orders, foreign_key: "shipping_address_id"
	belongs_to :country
    
    # An array of all the available types for an address
	TYPES = ["billing", "delivery"]
	EMAIL_REGEX = /\A[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}\z/i
    PHONE_REGEX = /\A[+?\d\ \-x\(\)]{7,}\z/

	# Validations
	validates :first_name, presence: true,
	                       length: {maximum: 100}
	validates :last_name, presence: true,
	                       length: {maximum: 100}
	validates :phone_number, presence: true,
	                       length: {within: 4..100}
	validates :email_address, presence: true,
	                       length: {within: 4..100},
	                       format: EMAIL_REGEX
	validates :address_type, presence: true, :inclusion => {:in => TYPES}
	validates :address1, presence: true
	validates :post_code, presence: true
	validates :country_id, presence: true

	# All addresses ordered by their id asending
    scope :ordered, -> { order(:id => :desc)}
    scope :default, -> { where(default: true)}
    scope :billing, -> { where(address_type: "billing")}
    scope :delivery, -> { where(address_type: "delivery")}
    
    def full_address
      # concatinates the addresses
      [address1, address2, address3, address4, post_code, country.try(:name)].join(", ")
    end

    def full_name
      "#{first_name.capitalize} #{last_name.capitalize}"
    end

end
