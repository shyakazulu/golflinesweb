class TaxRate < ActiveRecord::Base

	ADDRESS_TYPES = ['billing', 'delivery']
    
    # Validations
    validates :name, :presence => true
    validates :address_type, :inclusion => {:in => ADDRESS_TYPES}
    validates :rate, :numericality => true

    serialize :country_ids, Array
    before_validation { self.country_ids = self.country_ids.map(&:to_i).select { |i| i > 0} if self.country_ids.is_a?(Array) }
    
    # All products which are assigned to this tax rate
    has_many :products, :dependent => :restrict_with_exception, :class_name => 'Product'
    
    # All delivery service prices which are assigned to this tax rate
    has_many :delivery_service_prices, :dependent => :restrict_with_exception, :class_name => 'DeliveryServicePrice'
    
    # All tax rates ordered by their ID
    scope :ordered, -> { order(:id)}
    
    # Set the address type if appropriate
    before_validation { self.address_type = ADDRESS_TYPES.first if self.address_type.blank? }
    
    # A description of the tax rate including its name & percentage
    #
    # @return [String]
    def description
      "#{name} (#{rate}%)"
    end
    
    # The rate for a given order based on the rules on the tax rate
    #
    # @return [BigDecimal]
    def rate_for(order)
      return rate if countries.empty?
      return rate if address_type == 'billing'  && (order.billing_country.nil?   || country?(order.billing_country))
      return rate if address_type == 'delivery' && (order.delivery_country.nil?  || country?(order.delivery_country))
      BigDecimal(0)
    end
      
    def country?(id)
      id = id.id if id.is_a?(Country)
      self.country_ids.is_a?(Array) && self.country_ids.include?(id.to_i)
    end
    
    def countries
      return [] unless self.country_ids.is_a?(Array) && !self.country_ids.empty?
      Country.where(:id => self.country_ids)
    end

end
