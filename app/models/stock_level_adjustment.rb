class StockLevelAdjustment < ActiveRecord::Base

	# The orderable productwhich the stock level adjustment belongs to
    belongs_to :product
    # The parent (OrderItem) which the stock level adjustment belongs to
    # belongs_to :order_item
    # Validations
    # validates :description, :presence => true
    validates :adjustment_xs, :numericality => true
    validates :adjustment_s, :numericality => true
    validates :adjustment_m, :numericality => true
    validates :adjustment_l, :numericality => true
    validates :adjustment_xl, :numericality => true
    validates :adjustment_xxl, :numericality => true
    # All stock level adjustments ordered by their created date desending
    scope :ordered, -> { order(:id => :desc) }
    
end
