require "error"
require "errors/inapropriate_delivery_service"
class Order < ActiveRecord::Base
  #The associated delivered service
  belongs_to :delivery_service
  # The country where this order is being delivered to (if one has been provided)
  belongs_to :delivery_country, :class_name => 'Country', :foreign_key => 'delivery_country_id'
  
  # Set up a callback for use when an order is shipped
  define_model_callbacks :ship

  # Validations
  with_options :if => :separate_delivery_address? do |order|
    order.validates :shipping_address_id, :presence => true
  end

  validate do
    if self.delivery_required?
      if self.delivery_service.nil?
      	 errors.add :delivery_service_id, :must_be_specified
      elsif !self.valid_delivery_service?
        errors.add :delivery_service_id, :not_suitable
      end
    end
  end

  before_confirmation do
    # Ensure that before we confirm the order that the delivery service which has been selected
    # is appropritae for the contents of the order.
    if self.delivery_required? && !valid_delivery_service?
      raise Errors::InapropriateDeliveryService, :order => self
    end
  end

    # Create some delivery_ methods which will mimic the billing methods if the order does
    # not need a seperate address.
    [:delivery_name, :delivery_address1, :delivery_address2, :delivery_address3, :delivery_address4, :delivery_postcode, :delivery_country].each do |f|
      define_method(f) do
        separate_delivery_address? ? super() : send(f.to_s.gsub('delivery_', 'billing_'))
      end
    end

    def shipped?
      !!self.shipped_at?
    end

    def delivery_country
      self.separate_delivery_address ? self.shipping_address.country : self.billing_address.country
    end
    
    def total_weight
      order_items.inject(BigDecimal(0)) { |t,i| t + i.total_weight}
    end

    def delivery_required?
      total_weight > BigDecimal(0)
    end
    # An array of all the delivery services which are suitable for this order in it's
    # current state (based on its current weight)
    def available_delivery_services
      delivery_service_prices.map(&:delivery_service).uniq
    end

    # Return the delivery price for this order in its current state
    #
    # @return [BigDecimal]
    def delivery_service_price
      self.delivery_service && self.delivery_service.delivery_service_prices.for_weight(self.total_weight).first
    end
    
    def delivery_service_prices
       if delivery_required?
         prices = DeliveryServicePrice.joins(:delivery_service).where(:delivery_services => {:active => true}).order(:price).for_weight(total_weight)
         prices = prices.select { |p| p.countries.empty? || p.country?(self.delivery_country) }
         prices.sort{ |x,y| (y.delivery_service.default? ? 1 : 0) <=> (x.delivery_service.default? ? 1 : 0) } # Order by truthiness
       else
         []
       end
    end
    # The recommended delivery service for this order
    def delivery_service
      super || available_delivery_services.first
    end
     # The price for delivering this order in its current state
    # def delivery_price
    #   delivery_service_price.try(:price) || BigDecimal(0)
    # end
    # The cost of delivering this order in its current state
    def delivery_cost_price
      delivery_service_price.try(:cost_price) || BigDecimal(0)
    end
    # The tax rate for the delivery of this order in its current state
    def delivery_tax_rate
      delivery_service_price.try(:tax_rate).try(:rate_for, self) || BigDecimal(0)
    end
    # Is the currently assigned delivery service appropriate for this order?
    def valid_delivery_service?
      self.delivery_service ? self.available_delivery_services.include?(self.delivery_service) : !self.delivery_required?
    end
    # Remove the associated delivery service if it's invalid
    def remove_delivery_service_if_invalid
      unless self.valid_delivery_service?
        self.delivery_service = nil
        self.save
      end
    end
    # The URL which can be used to track the delivery of this order
    def courier_tracking_url
      return nil if self.shipped_at.blank? || @courier_tracking_url ||= self.delivery_service.tracking_url_for(self)
    end
    # Mark this order as shipped
    def ship!
      run_callbacks :ship do
        self.shipped_at = Time.now
        self.state = 'shipped'
        self.save!
      end
    end

end