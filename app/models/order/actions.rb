require "error"
require "errors/insufficient_stock_to_fulfil"
class Order < ActiveRecord::Base
    extend ActiveModel::Callbacks
    # These additional callbacks allow for applications to hook into other
    # parts of the order lifecycle.
    define_model_callbacks :confirmation, :acceptance, :rejection
  
    # This method should be called by the base application when the user has completed their 
    # first round of entering details. This will mark the order as "confirming" which means
    # the customer now just must confirm.
   
    def proceed_to_confirm(params = {})
      self.state = 'confirming'
      if self.update(params)
        true
      else
        false
      end
    end
  
    # This method should be executed by the application when the order should be completed
    # by the customer. It will raise exceptions if anything goes wrong or return true if
    # the order has been confirmed successfully
  
    def confirm!
      no_stock_of = self.order_items.select(&:validate_stock_levels)
      unless no_stock_of.empty?
        raise Errors::InsufficientStockToFulfil, :order => self, :out_of_stock_items => no_stock_of
      end
     
      run_callbacks :confirmation do
      	# Mark order as received
      	self.state = 'received'
        self.received_at = Time.now
        self.save!
        self.order_items.each(&:confirm!)
        #send an email to the customer
        deliver_received_order_email
        
      end
      true
    end

    # Mark order as accepted
    def accept!
      run_callbacks :acceptance do
        self.accepted_at = Time.now
        self.state = "accepted"
        self.save!
        deliver_accepted_order_email
      end
    end

    # Mark order as rejected

    def reject!
      run_callbacks :rejection do
        self.rejected_at = Time.now
        self.state = "rejected"
        self.save!
        deliver_rejected_order_email
      end
    end
    
    def pay!
      self.payments.create!(:amount => self.amount_paid, method: "default", confirmed: false, refundable: true, reference: "Order #{self.id}")  
    end

    def deliver_accepted_order_email
      OrderMailer.accepted(self).deliver_now
    end

    def deliver_rejected_order_email
      OrderMailer.rejected(self).deliver_now
    end

    def deliver_received_order_email
      OrderMailer.received(self).deliver_now
    end

end