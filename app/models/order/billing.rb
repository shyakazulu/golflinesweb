class Order < ActiveRecord::Base
  
  # The country which this order should be billed to
  belongs_to :billing_country, :class_name => 'Country', :foreign_key => 'billing_country_id'
  # Payments which have been stored for the order
  has_many :payments, :dependent => :destroy

  # Validations
  with_options :if => Proc.new { |o| !o.building? } do |order|
    order.validates :billing_address_id, :presence => true
  end
  
  def billing_name
     self.billing_address.full_name
  end

  def shipping_name
     self.shipping_address.full_name
  end
  # The total cost of the order
  def total_cost
    self.delivery_cost_price +
    order_items.inject(BigDecimal(0)) { |t, i| t + i.total_cost }
  end

  def items_sub_total
    order_items.inject(BigDecimal(0)) { |t, i| t + i.sub_total }
  end

  # The total amount due on the order
  #
  # @return [BigDecimal]
  def balance
    @balance ||= total - amount_paid
  end

  # Is there a payment still outstanding on this order?
  #
  # @return [Boolean]
  def payment_outstanding?
    balance > BigDecimal(0)
  end

  # Has this order been paid in full?
  #
  # @return [Boolean]
  def paid_in_full?
    !payment_outstanding?
  end

  # Is the order invoiced?
  #
  # @return [Boolean]
  def invoiced?
    !invoice_number.blank?
  end

end