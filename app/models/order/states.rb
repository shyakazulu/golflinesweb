class Order < ActiveRecord::Base
  
  # An array of all the available statees for an order
  STATES = ['building', 'confirming', 'received', 'accepted', 'rejected', 'shipped']

  # Validations
  validates :state, :inclusion => {:in => STATES}

  # Set the state to building if we don't have a state
  after_initialize  { self.state = STATES.first if self.state.blank? }
  #All orders which have been received
  scope :received, -> {where("received_at is not null")}
  # All orders which are currently pending acceptance/rejection
  scope :pending, -> { where(:state => 'received') }
  # All ordered ordered by their ID desending
  scope :ordered, -> { order(:id => :desc)}

   # Is this order still being built by the user?
    #
    # @return [Boolean]
    def building?
      self.state == 'building'
    end
  
    # Is this order in the user confirmation step?
    #
    # @return [Boolean]
    def confirming?
      self.state == 'confirming'
    end
  
    # Has this order been rejected?
    #
    # @return [Boolean]
    def rejected?
      !!self.rejected_at
    end
  
    # Has this order been accepted?
    #
    # @return [Boolean]
    def accepted?
      !!self.accepted_at
    end
  
    # Has the order been received?
    #
    # @return [Boolean]
    def received?
      !!self.received_at?
    end
    
end