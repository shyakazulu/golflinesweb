class Order < ActiveRecord::Base
	# Require dependencies
    require_dependency 'order/states'
    require_dependency 'order/actions'
    require_dependency 'order/billing'
    require_dependency 'order/delivery'

    PROMO_CODES = Promotion.visible.map(&:code) << nil

    #All items which make up this order
    has_many :order_items, :dependent => :destroy
    accepts_nested_attributes_for :order_items, :allow_destroy => true, :reject_if => Proc.new { |a| a['ordered_item_id'].blank? }
    has_many :products, :through => :order_items, :class_name => 'Product', :source => :ordered_item, :source_type => 'Product'
    belongs_to :customer, :class_name => "User"
    belongs_to :billing_address, class_name: "Address", :dependent => :destroy
    belongs_to :shipping_address, class_name: "Address", :dependent => :destroy

    # Validations
    validates :token, :presence => true
    validates :promo_code, :inclusion => {in: PROMO_CODES}
    # Set some defaults
    before_validation { self.token = SecureRandom.uuid  if self.token.blank?}
    before_validation { self.promo_code.strip}
    before_validation { self.promo_code = nil if self.promo_code.blank?}

   
    # The length of time the customer spent building the order before submitting it to us.
    # The time from first item in basket to received.
    scope :ordered, -> { order(:created_at => :desc)}
    def build_time
      return nil if self.received_at.blank?
      self.created_at - self.received_at
    end
    # Is this order empty? 
    def empty?
      order_items.empty?
    end
    # Does this order have items?
    def has_items?
      total_items > 0
    end

    # Return the number of items in the order
    def total_items
      order_items.inject(0) { |t,i| t + i.quantity }
    end

    def expired?
        return false if !self.building?
        if self.created_at.days_since(1) < Time.now
            self.state = "rejected"
            self.save!
            return true
        end
    end

end

