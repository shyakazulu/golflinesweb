class Currency < ActiveRecord::Base
	#relationships
	has_many :countries
	scope :ordered, -> { order(:created_at => :desc)}
    
    #validations
    validates :name, presence: true
    validates :rate, presence: true
end
