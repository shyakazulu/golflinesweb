class Product < ActiveRecord::Base
  # Add dependencies for products
  require_dependency 'product/product_attributes'
  require_dependency 'product/variants'

  #Attachments for this product
  has_many :attachments, :as => :parent, :dependent => :destroy, :autosave => true
  # The product's categorizations
  # has_many :product_categorizations, dependent: :destroy, inverse_of: :product
  # The product's categories
  belongs_to :product_category
  # Ordered items which are associated with this product
  has_many :order_items, dependent: :restrict_with_exception, :as => :ordered_item
  # Orders which contain this product
  has_many :orders, :through => :order_items
  # Stock level adjustments for this product
  has_many :stock_level_adjustments, :dependent => :destroy
  # Promotion if any
  belongs_to :promotion
  # Nested attributes for attachments
  accepts_nested_attributes_for :attachments, allow_destroy: true 
  # globalize gem initialization
  translates :name, :description
  #Validations
  validates :name, :presence => true
  validates :permalink, :presence => true, :uniqueness => true
  validates :weight, :numericality => true
  validates :price, :numericality => true


  # Before validation, set the permalink if we don't already have one
  before_validation { self.permalink = self.name.parameterize if self.permalink.blank? && self.name.is_a?(String) }
  before_validation { self.promo_price = self.price - self.promotion.rate * 0.01 * self.price if has_promotion }
 
  # All active Product
  scope :active, -> { where(active: true)}
  scope :ordered, -> { order(:created_at => :desc)}
  scope :category, -> (category) {where product_category_id: category}
  
  def orderable?
    return false unless self.active?
    return false if self.has_variants?
    true
  end

  #The price of the product
  def current_price
  	self.promoted ? self.promo_price : read_attribute(:price)
  end
  
  def in_stock?
    stock_control? ? stock > 0 : true
  end

  # Return the total number of items currently in stock
  def stock_lg
    self.stock_level_adjustments.sum(:adjustment_l) 
  end

  def stock_xl
    self.stock_level_adjustments.sum(:adjustment_xl)
  end

  def stock_xxl
    self.stock_level_adjustments.sum(:adjustment_xxl)
  end

  def stock_m
    self.stock_level_adjustments.sum(:adjustment_m)
  end
  
  def stock_sm
    self.stock_level_adjustments.sum(:adjustment_s)
  end

  def stock_xs
    self.stock_level_adjustments.sum(:adjustment_xs)
  end

  def total_stock
    stock_lg +  stock_xl + stock_xxl +  stock_m + stock_xs 
  end

  def stock(size)
    case size
    when 's'
      stock_sm
    when 'm'
      stock_m
    when 'l'
      stock_lg
    when 'xl'
      stock_xl
    when 'xxl'
      stock_xxl
    when 'xs'
      stock_xs
    end
  end
  
  def has_attachments?
    self.attachments.count > 0 
  end
  
  def has_promotion
    !self.promotion.nil?
  end

  def has_at_least_one_product_category
    errors.add(:base, 'must add at least one product category') if self.product_categories.blank?
  end

end
