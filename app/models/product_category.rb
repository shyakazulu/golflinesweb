class ProductCategory < ActiveRecord::Base
	# Relationships
	has_many :attachments, :as => :parent, :dependent => :destroy
	# has_many :product_categorizations, inverse_of: :product_category
  has_many :products
  # , through: :product_categorizations
  belongs_to :promotion
  
  # Validations
  validates :name, :presence => true
  validates :permalink, :presence => true, :uniqueness => true
  validates :description, :presence => true,
                          length: {within: 1..3000}
  # globalize gem initialization
  translates :name, :description
  
  before_validation :set_permalink
  before_validation :promote

  scope :ordered, -> {order(:created_at => :desc)}

  def image
    self.attachments.for("image")
  end

  def has_promotion
    !self.promotion.nil?
  end

  private

  def set_permalink
    self.permalink = self.name.parameterize if self.permalink.blank? && self.name.is_a?(String)
  end

  def promote
    if has_promotion
      self.products.each do |product|
        product.update_attributes(promotion_id: self.promotion.id, promoted: true)
      end
    end
  end
    
end
