class Product < ActiveRecord::Base
  # Validations
  validate {error.add :base, :can_belong_to_root if self.parent && self.parent.parent }
  has_many :variants, :class_name => 'Product', :foreign_key => 'parent_id', :dependent => :destroy

  # The parent product (only applies to variants)
  belongs_to :parent, :class_name => 'Product', :foreign_key => 'parent_id'
  # All products which are not variants
  scope :root, -> { where(:parent_id => nil) }

  # Does this product have any variants?
  def has_variants?
    !variants.empty?
  end

  # Returns the default variant for the product or nil if none exists.
  def default_variant
    return nil if self.parent
    @default_variant ||= self.variants.select { |v| v.default? }.first
  end
  
  # Is this product a variant of another?
  def variant?
    !self.parent_id.blank?
  end

end