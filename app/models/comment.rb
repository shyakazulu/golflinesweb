class Comment < ActiveRecord::Base
	#relationships
	belongs_to :article
	belongs_to :customer, :class_name => "User"
	belongs_to :parent, :class_name => "Comment", :dependent => :destroy
    has_many :evaluations, class_name: "RSEvaluation", as: :target

	has_reputation :likes, source: :user, aggregated_by: :sum
    
    #validations
    validates :content, presence: true,
                        length: {maximum: 500}
    
	scope :sorted, -> {order(:id => :desc)}

	#determines wether user has liked an item already
    def was_liked?(item)
      evaluations.where(source_type: item.class, source_id: item.id).present?
    end
end
