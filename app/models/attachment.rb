class Attachment < ActiveRecord::Base
	#relationships
	belongs_to :parent, :polymorphic => true
	# Mount the Carrierwave uploader
    mount_uploader :file, AttachmentUploader

    ATTACHMENTS = ['front', 'back', 'detail_front', 'detail_back', 'detail_back_lg', 'detail_front_lg', 'head', 'avatar', 'cover']

    # Validations
    validates :file_name, :presence => true,
                          :inclusion => {in: ATTACHMENTS}
    validates :file_type, :presence => true
    validates :file_size, :presence => true
    validates :file, :presence => true
    validates :token, :presence => true, :uniqueness => true

    # All attachments should have a token assigned to this
    before_validation { self.token = SecureRandom.uuid if self.token.blank? }
    # Set the appropriate values in the model
    before_validation do
      if self.file
        self.file_name = self.file.filename[0..-5] if self.file_name.blank?
        self.file_type = self.file.content_type if self.file_type.blank?
        self.file_size = self.file.size if self.file_size.blank?
      end
    end
    #Return the path to the attachement
    def path
    	file.url
    end
    #Is the attachement an image?
    def image?
      file_type.match(/\Aimage\//).present?
    end

end
