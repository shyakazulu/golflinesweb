class User < ActiveRecord::Base

  # Setting up Rolify gem 
  rolify
  # Relationships
  has_many :addresses, foreign_key: "customer_id"
  has_many :articles, foreign_key: "admin_id"
  has_many :orders, foreign_key: "customer_id"
  has_many :comments, foreign_key: "customer_id"
  has_many :attachments, :as => :parent, :dependent => :destroy, :autosave => true

  has_secure_password
  # before_create {generate_token(:password_reset_token)}
  # Validations
  EMAIL_REGEX = /\A[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}\z/i
  validates :email, presence: true,
                    uniqueness: true,
                    length: {within: 4..150},
                    format: EMAIL_REGEX

  validates :first_name, presence: true,
                         length: {maximum: 100}
  validates :last_name,  presence: true,
                         length: {maximum: 100}
   validates :job_title, length: {maximum: 100}
   validates :facebook, length: {maximum: 100}
   validates :twitter, length: {maximum: 100}
   validates :instagram, length: {maximum: 100}
  
  scope :ordered, -> { order(:id => :desc)}
  # scope :admin, -> {where(self.has_role? :admin)}
  # send welcome email
  after_create :send_welcome_email

  # The user's first name & last name concatenated
  def full_name
    "#{first_name.capitalize} #{last_name.capitalize}"
  end
  
  # The user's first name & initial of last name concatenated
  def short_name
    "#{first_name} #{last_name[0,1]}".titleize
  end

  def add_customer
    self.add_role :customer
  end

  def add_admin
    self.add_role :admin
  end
  
  def is_admin?
    self.has_role? :admin
  end

  def has_attachments?
    self.attachments.count > 0 
  end
  def has_payment_info?
    braintree_customer_id
  end
  # Sending password reset email
  def send_password_reset
     generate_token(:password_reset_token)
     self.password_resent_token_sent_at = Time.zone.now
     save!
     UserMailer.password_reset(self).deliver
  end 

  def send_welcome_email
    UserMailer.welcome(self).deliver
  end

  # Attempt to authenticate a user based on email & password. Returns the 
  # user if successful otherwise returns false.
  def self.authenticate(email_address, password)
    user = self.where(:email_address => email_address).first
    return false if user.nil?
    return false unless user.authenticate(password)
    user
  end

  def generate_token(column)
    begin
      self[column] = SecureRandom.urlsafe_base64
    end while User.exists?(column => self[column])
  end
end
