require "error"
require "errors/not_enough_stock"
class OrderItem < ActiveRecord::Base
  belongs_to :order, :class_name => 'Order', :touch => true, :inverse_of => :order_items
  belongs_to :ordered_item, :polymorphic => true

  # Validations
  validates :quantity, :numericality => true
  validates :ordered_item, :presence => true
  
  validate do
    unless in_stock?
      errors.add :quantity, :too_high_quantity
    end
  end
  
  # After saving, if the order has been shipped, reallocate stock appropriate
  after_save do
    if order.shipped?
      allocate_unallocated_stock(self.size)
    end
  end
  # # Check for promotion code
  # before_validation :check_promo
  # Remove a product from an order. It will also ensure that the order's custom delivery
  # service is updated if appropriate.
  def remove
    transaction do
      self.destroy!
      self.order.remove_delivery_service_if_invalid
      self
    end   
  end
  # Increases the quantity of items in the order by the number provided. Will raise an error if we don't have
  # the stock to do this
  def increase!(amount = 1)
  	transaction do
      self.quantity += amount
      unless self.in_stock?
        raise Errors::NotEnoughStock, :ordered_item => self.ordered_item, :requested_stock => self.quantity
      end
      self.save!
      self.order.remove_delivery_service_if_invalid
  	end
  end
  # Decreases the quantity of items in the order by the number provided.

  def decrease!(amount = 1)
  	transaction do
      self.quantity -= amount
      self.quantity == 0 ? self.destroy : self.save!
  	end
  end

  # The total weight of the item
  def weight
    read_attribute(:weight) || ordered_item.try(:weight) || BigDecimal(0)
  end
  
  # Return the total weight of the item
  def total_weight
    quantity * weight
  end

  #the unit price for the item
  def unit_price
    read_attribute(:unit_price) || ordered_item.try(:price) || BigDecimal(0)
  end

  # The cost price for the item
  def unit_cost_price
    read_attribute(:unit_price) || ordered_item.try(:cost_price) || BigDecimal(0)
  end

  #The total cost of the product
  def total_cost
    quantity * unit_cost_price
  end
  # The sub total for the product
  def sub_total
    quantity * unit_price
  end
  
  def confirm!
    allocate_unallocated_stock(self.size)
  end
  # Do we have the stock needed to fulfil this order?
  def in_stock?
  	if self.ordered_item && self.ordered_item.stock_control?
  	  self.ordered_item.stock(self.size) >= self.quantity
  	else
      false	
  	end
  end
  # How much stock has been allocated to this item?
  def available_stock
    self.ordered_item.stock(self.size)
  end
  # Validate the stock level against the product and update as appropriate. This method will be executed
  # before an order is completed. If we have run out of this product, we will update the quantity to an
  # appropriate level (or remove the order item) and return the object.
  def validate_stock_levels
    if in_stock?
      false
    else
      self.quantity = self.ordered_item.stock(self.size)
      self.quantity == 0 ? self.destroy : self.save!
      self
    end
  end

  def check_promo
    if !self.order.promo_code.nil?
      code = self.order.promo_code
      rate = Promotion.find_by_code(code).rate
      price = self.ordered_item.price
      self.unit_price = price - rate * 0.01 * price
      self.save!
      true
    else
      false
    end
  end
  
  # Allocate any unallocated stock for this order item.
  def allocate_unallocated_stock(type)
  	if self.ordered_item.stock_control? && self.available_stock != 0
      case type
        when 'xs'
          self.ordered_item.stock_level_adjustments.create!(:adjustment_xs => 0 - self.quantity, :description => "Order #{self.order.id}")
          true
        when 's'
          self.ordered_item.stock_level_adjustments.create!(:adjustment_s => 0 - self.quantity, :description => "Order #{self.order.id}")
          true
        when 'm'
          self.ordered_item.stock_level_adjustments.create!(:adjustment_m => 0 - self.quantity, :description => "Order #{self.order.id}")
          true
        when 'l'
          self.ordered_item.stock_level_adjustments.create!(:adjustment_l => 0 - self.quantity, :description => "Order #{self.order.id}")
          true
        when 'xl'
          self.ordered_item.stock_level_adjustments.create!(:adjustment_xl => 0 - self.quantity, :description => "Order #{self.order.id}")
          true
        when 'xxl'
          self.ordered_item.stock_level_adjustments.create!(:adjustment_xxl => 0 - self.quantity, :description => "Order #{self.order.id}")
          true
      end  		
  	end
  end

end
