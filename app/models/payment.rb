class Payment < ActiveRecord::Base
	# Additional callbacks
    extend ActiveModel::Callbacks
    define_model_callbacks :refund

    belongs_to :order
    belongs_to :parent, :class_name => "payment", :foreign_key => "parent_payment_id"

    # validations
    validates :amount, numericality: true
    validates :reference, presence: true
    validates :method, presence: true
    
    # Is this payment a refund?
    def refund?
      self.amount < BigDecimal(0)
    end
    # How much of the payment can be refunded
    def refundable_amount
      refundable? ? (self.amount - self.amount_refunded) : BigDecimal(0)
    end

    def transaction_url
      nil
    end

    def state
        self.confirmed ? "Confirmed" : "Pending"
    end

    def confirm!
        self.confirmed = true
        self.save!
    end

end
