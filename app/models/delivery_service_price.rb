  class DeliveryServicePrice < ActiveRecord::Base

    # include AssociatedCountries
    serialize :country_ids
    # The delivery service which this price belongs to
    belongs_to :delivery_service
    #validations
    validates :code, :presence => true
    validates :country_ids, presence: true
    validates :price, presence: true,
                     :numericality => true
    validates :cost_price, :numericality => true,
                           :allow_blank => true
    validates :min_weight,:presence => true,
                          :numericality => true
    validates :max_weight, :presence => true,
                           :numericality => true

    before_validation { self.country_ids = self.country_ids.map(&:to_i).select { |i| i > 0} if self.country_ids.is_a?(Array) }

    # All prices ordered by their price ascending
    scope :ordered, -> { order(:price => :asc) }
    # All prices which are suitable for the weight passed.
    scope :for_weight, -> weight { where("min_weight <= ? AND max_weight >= ?", weight, weight) }
    
    def country?(id)
      id = id.id if id.is_a?(Country)
      self.country_ids.is_a?(Array) && self.country_ids.include?(id.to_i)
    end
    
    def countries
      return [] unless !self.country_ids.empty?
      Country.where(:id => self.country_ids)
    end
    
  end
