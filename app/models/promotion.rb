class Promotion < ActiveRecord::Base
	# Relationships
	has_many :products
	has_many :product_categories
	has_many :attachments, :as => :parent, :dependent => :destroy, :autosave => true
    
	# Validations
	validates :rate, presence: true
	validates :description, presence: true,
	                        length: {maximum: 200}
    validates :valid_until, presence: true
    validates :code, uniqueness: true,
    					   length: {maximum: 20}
	# globalize gem initialization
	translates :description

    after_destroy :reset_products

    scope :code, -> (code) {where code: code}
    scope :visible, -> { where(visible: true)}
    scope :ordered, -> { order(:created_at => :desc)}

	def reset_products
		self.products.each do |product|
          product.update_attributes(promotion_id: nil)
		end
		self.product_categories do |category|
          category.update_attributes(promotion_id: nil)
		end
	end

	def active
      self.visible && self.valid_until > DateTime.now
	end

	def has_attachments?
      self.attachments.count > 0 
    end
    

end
