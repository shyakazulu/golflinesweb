class BlogController < ApplicationController
	def blog
		@articles = Article.published.page(params[:page]).per_page(20)
        @article = Article.published.last
	end

	def article 
		if logged_in? && current_user.is_admin?
		  @article = Article.find_by_permalink(params[:permalink])
		else
          @article = Article.published.find_by_permalink(params[:permalink])
		end
		@comments = @article.comments.sorted
		if logged_in?
		  @comment = Comment.new
		end
	end
end
