class PasswordResetsController < ApplicationController
  layout 'authentication'
  def new
  end

  def create
  	user = User.find_by_email(params[:email])
  	user.send_password_reset if user
  	redirect_to root_url
    flash[:success] = "#{t 'general.reset_instructions'}"
  end

  def edit
  	@user = User.find_by_password_reset_token!(params[:id])
  end

  def update
  	@user = User.find_by_password_reset_token!(params[:id])
  	if @user.password_resent_token_sent_at < 2.hours.ago
        redirect_to new_password_reset_path
        flash[:alert] = "#{t 'general.reset_expired'}"
    elsif @user.update_attributes(password_reset_params)
    	redirect_to root_url
    	flash[:notice] = "Your password has been reset!"
    else
    	render :edit 
  	end
  end

  private

  def password_reset_params
  	params.require(:user).permit(:password, :password_confirmation)
  end
end
