class ProcessController < ApplicationController

  def delivery
    @order = current_user.orders.find_by_token(params[:token])
    @delivery_services = @order.available_delivery_services
  end
end
