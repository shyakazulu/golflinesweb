class UserSessionsController < ApplicationController
  layout 'authentication'
  def new
  end
  
  def create
  	user = User.find_by(email: params[:email])
  	if user && user.authenticate(params[:password])
      if params[:remember_me]
        signed_token = Rails.application.message_verifier(:remember_me).generate(user.id)
        cookies.permanent.signed[:remember_me_token] = signed_token
      end
      session[:user_id] = user.id
      session[:expires_at] = Time.current + 24.hours
      flash[:success] = "Thanks for logging in!"
      redirect_to root_path 
    else
      flash[:error] = "Please reenter your email and password."
      redirect_to login_path
    end
  end

  def destroy
    session[:user_id] = nil
    session[:expires_at] = nil
    session[:cart] = nil
    cookies.delete(:remember_me_token)
    reset_session
    redirect_to root_path, notice: "you have been logged out."
  end
end
