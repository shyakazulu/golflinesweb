class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :set_locale

  private
  
  def logged_in?
    current_user
  end
  helper_method :logged_in?

  def current_user
    if session[:user_id]
      @current_user ||= User.find(session[:user_id])
    elsif cookies.permanent.signed[:remember_me_token]
      verification = Rails.application.message_verifier(:remember_me).verify(cookies.permanent.signed[:remember_me_token])
      if verification
        Rails.logger.info "Logging in by cookie."
        @current_user ||= User.find(verification)
      end
    end
  end
  helper_method :current_user

  def cart
    if session[:cart]
      @cart ||= session[:cart]
    else
      @cart = {}
    end
  end
  helper_method :cart

  def cart_total
    bag_total = 0
    cart.each do |i, item|
      total = item["price"].to_i * item["quantity"].to_i
      bag_total += total
    end
    bag_total
  end
  helper_method :cart_total
  def cart_weight
    bag_weight = 0
    cart.each do |i, item|
      total = item["weight"].to_i * item["quantity"].to_i
      bag_weight += total
    end
    bag_weight
  end
  helper_method :cart_weight


  def require_user
    if current_user
      if session[:expires_at] < Time.current
        redirect_to logout_path
      else
        true
      end
    else
      redirect_to login_path, notice: "You must be logged in to access that page."
    end
  end

  def require_admin_user
    if current_user && current_user.is_admin?
      true
    else
      not_found
    end
  end

  def set_locale
    I18n.locale = params[:locale] if params[:locale].present?
  end
  #raises 404 page not found error
  def not_found
    raise ActionController::RoutingError.new('Not Found')  
  end

  def default_url_options(options = {})
    {locale: I18n.locale}
  end
end
