class CommentsController < ApplicationController
  before_action :require_user, except: ['destroy']
  before_action :require_admin_user, only: ['destroy']
  before_action :set_comment, only: ['destroy', 'like']
  
  def new
  end

  def create
  	@comment = current_user.comments.new(comment_params)
 	  respond_to do |format|
        if @comment.save
          format.html {
            redirect_to :back
          }
          format.js
        else
          format.html { 
            render action: 'new'
          }
          format.js
        end
	  end
  end

  def destroy
  	@comment.destroy
    flash[:success] = "The comment has been Deleted."
    redirect_to :back
  end

  def like
    @comment.add_evaluation(:likes, 1, current_user)

    respond_to do |format|
      format.html {
        redirect_to :back
      }
      format.js
    end
  end

  private
    def comment_params
    	params.require(:comment).permit(:customer_id, :content, :likes, :parent_id, :article_id)
    end

    def set_comment
      @comment = Comment.find(params[:id])
    end
end
