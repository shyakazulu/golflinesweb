class Admin::UsersController < ApplicationController
  before_action :set_user, only: [:update, :edit, :destroy]
  before_action :require_admin_user, except: [:register, :create]
  layout :layout_by_action_name
  
  def index
    @users = User.with_role(params[:role]).page(params[:page]).per_page(30)
  end

  def new
    @user = User.new 
  end

  def edit
  end
  
	def create
	  @user = User.new(user_params)
 	  respond_to do |format|
        if @user.save
          if params[:role] == "admin"
            @user.add_role :admin
            if params[:images]
              params[:images].each {|image|
                @user.attachments.create(file: image, parent_type: 'user')
              }
            end
            format.html { 
              flash[:success] = "A new user has been created."
              redirect_to admin_users_path(role: "admin")
            }
            format.js
          else
            @user.add_role :customer
            format.html { 
              flash[:success] = "Thanks for registering! please sign in."
              redirect_to login_path()
            }
            format.js
          end
        else
          format.html { 
            flash[:alert] = "Something went wrong."
            if @user.is_admin?
              render action: 'new'
            else
              redirect_to login_path()
            end
          }
          format.js
        end
	  end
	end

	def update
	  if @user.update_attributes(user_params)
        if params[:images]
          params[:images].each {|image|
            if !(@user.attachments.find_by_file_name(image.original_filename[0..-5].to_s).nil?)
              @user.attachments.find_by_file_name(image.original_filename[0..-5].to_s).destroy 
            end
            @user.attachments.create(file: image, parent_type: 'user')
          }
        end
        flash[:success] = "The user has been updated."
        redirect_to admin_users_path(role: "admin")
      else
        flash[:alert] = "Something went wrong."
      	render action: 'edit'
	  end
	end

  def register
    @user = User.new 
  end

  def destroy
    @user.destroy
    flash[:success] = "The User has been Deleted."
    redirect_to admin_users_path
  end

  def layout_by_action_name
    if ["register"].include? action_name
      "authentication"
    else
      "admin"
    end
  end

  private

  def user_params
    params.require(:user).permit(:first_name, :last_name, :phone, :avatar, :email, :password, :job_title, :facebook, :instagram, :twitter)
  end

  def set_user
    @user = User.find(params[:id])  
  end
end
