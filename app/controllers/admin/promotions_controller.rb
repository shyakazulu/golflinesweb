class Admin::PromotionsController < ApplicationController
  before_action :set_promo, only: [:update, :edit, :destroy]
  before_action :require_admin_user
  layout 'admin'

  def index
  	@promotions = Promotion.ordered.page(params[:page]).per_page(30)
  end

  def new
  	@promotion = Promotion.new  
  end

  def edit
  end

  def create
  	@promotion = Promotion.new(promo_params)
  	if @promotion.save
      if params[:images].each{|image|
        @promotion.attachments.create(file: image, parent_type: 'promotion')
      } 
      end
      flash[:success] = "A new promotion has been created."
      redirect_to admin_promotions_path
    else
      flash[:alert] = "Something went wrong."
      render action: 'new'
  	end
  end

  def update
  	if @promotion.update_attributes(promo_params)
      if params[:images] 
        params[:images].each {|image|
          if !(@promotion.attachments.find_by_file_name(image.original_filename[0..-5].to_s).nil?)
            @promotion.attachments.find_by_file_name(image.original_filename[0..-5].to_s).destroy 
          end
          @promotion.attachments.create(file: image, parent_type: 'promotion')
        }
      end
      flash[:success] = "The promotion has been updated."
        redirect_to admin_promotions_path
      else
        flash[:alert] = "Something went wrong."
      	render action: 'edit'
  	end
  end

  def destroy
  	@promotion.destroy
    flash[:success] = "The Promotion has been Deleted."
    redirect_to admin_promotions_path
  end

  private
     
    def promo_params
      params.require(:promotion).permit(:rate, :visible, :description, :valid_until, :code)
    end

    def set_promo
      @promotion = Promotion.find(params[:id])  
    end

end
