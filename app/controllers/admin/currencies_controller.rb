class Admin::CurrenciesController < ApplicationController
	before_action :set_currency, only: [:update, :edit, :destroy]
	before_action :require_admin_user
    layout 'admin'

	def index
	  @currencies = Currency.all.page(params[:page]).per_page(30)
	end

	def new
	  @currency = Currency.new
	end

	def create
      @currency = Currency.new(currency_params)
      if @currency.save
        flash[:success] = "A new currency has been created."
        redirect_to admin_currencies_path
      else
      	flash[:alert] = "Something went wrong."
        render action: 'new'
      end
	end

    def edit	
    end

	def update
	  if @currency.update_attributes(currency_params)
        flash[:success] = "The currency has been updated."
        redirect_to admin_currencies_path
      else
        flash[:alert] = "Something went wrong."
        render action: 'edit'
      end
	end

	def destroy
	  @currency.destroy
	  flash[:success] = "The Currency has been Deleted."
      redirect_to admin_currencies_path
	end

	private 

	def currency_params
	   params.require(:currency).permit(:name, :rate, :locale)
	end

	def set_currency
	  @currency = Currency.find(params[:id])
	end
end
