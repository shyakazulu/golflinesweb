class Admin::AddressesController < ApplicationController
	before_action :require_user
    def index
        @addresses = Address.all
    end

	def new
		@address = current_user.addresses.new
	end

	def create
		@address = current_user.addresses.new(address_params)
		if @address.save
			if @address.default
			    flash[:notice] = "Do you want a separate delivery address?"
			    order = current_user.orders.create(:billing_address_id => @address.id, :state => "building", separate_delivery_address: false)
			    order.save!
	            redirect_to set_address_path(delivery: true, token: order.token)
	            # creating order_items for order
	            session[:cart].each do |i, item|
		    	    order_item = order.order_items.create(:ordered_item_id => item["id"].to_i, :ordered_item_type => "Product", :quantity => item["quantity"], :unit_price => item["price"], :weight => item["weight"], :size => item["size"])
		    	    order_item.save!
		    	end
	        else
	        	flash[:notice] = "Almost done, please choose a courier."
	        	order = current_user.orders.find_by_token(params[:token])
	        	if order.expired?
	        		flash[:notice] = "Your order has expired, please try again."
	        		redirect_to root_path
	        	else
    				order.update_attributes(:shipping_address_id => @address.id, separate_delivery_address: true)
	           	    order.save!
	            	redirect_to set_delivery_path(token: params[:token])
	        	end
	        end
	      else
	      	if @address.default
			    flash[:bill] = "Something went wrong."
	            render action: 'new'
	        else
	        	flash[:ship] = "Something went wrong."
	            render action: 'new'
	        end
		end
	end

	private

	def address_params
		params.require(:address).permit(:customer_id, :address_type, :default, :address1, :address2, :address3, :address4, :post_code, :country_id, :first_name, :last_name, :email_address, :phone_number, :order_id)
	end
end
