class Admin::ArticlesController < ApplicationController
  before_action :set_article, only: [:update, :edit, :destroy]
  before_action :require_admin_user, except: [:like]
  before_action :require_user, only: [:like]
  layout 'admin'

	def index
	  @articles = Article.ordered.page(params[:page]).per_page(30)
	end

	def new
	  @article = current_user.articles.new
	end

  def edit
  end

	def create
	  @article = current_user.articles.new(article_params)
 	  respond_to do |format|
        if @article.save
          if params[:images]
            params[:images].each {|image|
              @article.attachments.create(file: image, parent_type: 'article')
            }
          end
          format.html { 
            flash[:success] = "A new article has been created."
            redirect_to admin_articles_path
          }

          format.js
        else
          format.html { 
            flash[:alert] = "Something went wrong."
            render action: 'new'
          }
          format.js
        end
	  end
	end

	def update
	  if @article.update_attributes(article_params)
        if params[:images]
          params[:images].each {|image|
            if !(@article.attachments.find_by_file_name(image.original_filename[0..-5].to_s).nil?)
              @article.attachments.find_by_file_name(image.original_filename[0..-5].to_s).destroy 
            end
            @article.attachments.create(file: image, parent_type: 'article')
          }
        end
        flash[:success] = "The article has been updated."
        redirect_to admin_articles_path
      else
        flash[:alert] = "Something went wrong."
      	render action: 'edit'
	  end
	end

  def destroy
    @article.destroy
    flash[:success] = "The Article has been Deleted."
    redirect_to admin_articles_path
  end

  def like
    @article = Article.find(params[:id])
    @article.add_or_update_evaluation(:likes, 1, current_user)

    respond_to do |format|
      format.html {
        redirect_to :back
      }
      format.js
    end
  end

	private
     
    def article_params
      params.require(:article).permit(:title, :permalink, :content, :admin_id, :likes, :credits, :pub_date, :visible)
    end

    def set_article
      @article = Article.find(params[:id])  
    end
end
