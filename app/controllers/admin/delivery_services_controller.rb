class Admin::DeliveryServicesController < ApplicationController
  before_action :set_delivery_service, only: [:update, :edit, :destroy]
  before_action :require_admin_user
  layout 'admin'

  def index
  	@delivery_services = DeliveryService.ordered.page(params[:page]).per_page(30)
  end

  def new
  	@delivery_service = DeliveryService.new
  end

  def edit
  end

  def create
  	@delivery_service = DeliveryService.new(delivery_service_params)
  	if @delivery_service.save
      flash[:success] = "A new Delivery Service has been created."
      redirect_to admin_delivery_services_path
    else
      flash[:alert] = "Something went wrong."
      render action: 'new'
  	end
  end

  def update
  	if @delivery_service.update_attributes(delivery_service_params)
      flash[:success] = "The Delivery Service has been updated."
        redirect_to admin_delivery_services_path
      else
        flash[:alert] = "Something went wrong."
        render action: 'edit'
  	end
  end

  def destroy
    @delivery_service.destroy
    flash[:success] = "The Delivery Service has been Deleted."
    redirect_to admin_delivery_services_path
  end

  private

  def delivery_service_params
  	params.require(:delivery_service).permit(:name, :default, :active, :tracking_url, :courier)
  end

  def set_delivery_service
  	@delivery_service = DeliveryService.find(params[:id])
  end

end
