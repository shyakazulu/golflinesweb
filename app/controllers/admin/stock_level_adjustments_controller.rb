class Admin::StockLevelAdjustmentsController < ApplicationController
  before_action :require_admin_user
  before_action :set_product, only: [:new, :create]

  def index
  end

  def new
  	@stock_level_adjustments = @product.stock_level_adjustments.limit(3)
  	@stock_level_adjustment = @product.stock_level_adjustments.new
  	respond_to do |format|
      format.html
      format.js
   end
  end

  def create
  	@stock_level_adjustment = @product.stock_level_adjustments.new(sla_params)
  	if @stock_level_adjustment.save
       respond_to do |format|
         format.html { 
            redirect_to admin_products_path
            flash[:success] = 'The stock Level was successfully updated.'
         }

         format.js
       end
  	end
  end

  private

  def set_product
  	@product = Product.find(params[:product_id])
  end

  def sla_params
  	params.require(:stock_level_adjustment).permit(:item_id, :description, :adjustment_s, :adjustment_xs, :adjustment_m, :adjustment_l, :adjustment_xl, :adjustment_xxl)
  end

end
