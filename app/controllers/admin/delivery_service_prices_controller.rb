class Admin::DeliveryServicePricesController < ApplicationController
  before_action :set_delivery_service
  before_action :set_delivery_service_price, only: [:update, :edit, :destroy]
  before_action :require_admin_user
  layout 'admin'

  def index
    @delivery_service_prices = @delivery_service.delivery_service_prices.ordered.page(params[:page]).per_page(30)
  end

  def new
    @delivery_service_price = @delivery_service.delivery_service_prices.new
  end

  def edit
  end

  def create
    @delivery_service_price = @delivery_service.delivery_service_prices.new(delivery_service_price_params)
    if @delivery_service_price.save
      flash[:success] = "A new Delivery Service price has been created."
      redirect_to admin_delivery_service_delivery_service_prices_path
    else
      flash[:alert] = "Something went wrong."
      render action: 'new'
    end
  end

  def update
    if @delivery_service_price.update_attributes(delivery_service_price_params)
      flash[:success] = "The Delivery Service price has been updated."
      redirect_to admin_delivery_service_delivery_service_prices_path
    else
      flash[:alert] = "Something went wrong."
      render action: 'new'
    end
  end

  def destroy
    @delivery_service_price.destroy
    flash[:success] = "The Product Delivery Service has been Deleted."
    redirect_to admin_delivery_service_delivery_service_prices_path
  end

  private

  def delivery_service_price_params
    params.require(:delivery_service_price).permit(:delivery_service_id, :code, :price, :cost_price, :min_weight, :max_weight, :country_ids => [])
  end

  def set_delivery_service
    @delivery_service = DeliveryService.find(params[:delivery_service_id])
  end
  
  def set_delivery_service_price
    @delivery_service_price = DeliveryServicePrice.find(params[:id])
  end
end
