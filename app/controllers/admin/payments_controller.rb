class Admin::PaymentsController < ApplicationController
	before_action :require_user
	before_action :set_order, except: [:confirm]

    def new
    	gon.client_token = generate_client_token
    end

    def create
    	currency = Currency.find_by_locale("en")
        cost = currency.rate * @amount_paid.to_i

        unless current_user.has_payment_info?
			@result = Braintree::Transaction.sale(
    		amount: cost.to_i,
    		payment_method_nonce: params[:payment_method_nonce],
    		customer: {
              first_name: current_user.first_name,
              last_name: current_user.last_name,
              company: "Not required",
              email: current_user.email,
              phone: "Not required"
            },
            options: {
              store_in_vault: true,
              :submit_for_settlement => true
            })      
        else
    		@result = Braintree::Transaction.sale(
    		amount: @amount_paid,
    		payment_method_nonce: params[:payment_method_nonce])
        end

    	if @result.success?
            current_user.update(braintree_customer_id: @result.transaction.customer_details.id) unless current_user.has_payment_info?
    		@order.update_attributes(:amount_paid => @amount_paid)
    		@order.confirm!
    		@order.pay!
    		session[:cart] = nil
    		flash[:notice] = "Congratulations! Your transaction has successfully completed."
    		redirect_to root_url
    	else
    		flash[:alert] = "Something went wrong while proccessing your transaction. Please try again!"
            gon.client_token = generate_client_token
            render :new
    	end
    end

    def confirm
    	@payment = Payment.find(params[:id])
    	@payment.confirm!
    	flash[:success] = "This payment has been marked as confirmed."
    	redirect_to admin_order_path(@payment.order)
    end

	private

    def set_order
    	if Order.find_by_token(params[:token]).expired?
    		flash[:notice] = "Your order has expired, please try again."
    		redirect_to root_path
        else
        	@order = Order.find_by_token(params[:token])
	   		@amount_paid = @order.total_cost
        end
    end

    def generate_client_token
	    if current_user.has_payment_info?
	        Braintree::ClientToken.generate(customer_id: current_user.braintree_customer_id)
	    else
	        Braintree::ClientToken.generate
	    end
	end

	def payment_params
	   params.require(:payment).permit(:order_id, :amount, :method, :confirmed, :refundable, :amount_refunded, :parent_payment_id, :reference)
	end
end
