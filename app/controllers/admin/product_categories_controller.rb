class Admin::ProductCategoriesController < ApplicationController
  before_action :set_category, only: [:update, :edit, :destroy]
  before_action :require_admin_user
  layout 'admin'

  def index
    @product_categories = ProductCategory.ordered.page(params[:page]).per_page(30)
  end

  def new
    @product_category = ProductCategory.new
  end

  def edit
  end

  def create
    @product_category = ProductCategory.new(product_category_params)
    if @product_category.save
      flash[:success] = "A new category has been created."
      redirect_to admin_product_categories_path
    else
      flash[:alert] = "Something went wrong."
      render action: 'new'
    end
  end
  
  def update
    if @product_category.update_attributes(product_category_params)
      flash[:success] = "The product category has been updated."
      redirect_to admin_product_categories_path
    else
      flash[:alert] = "Something went wrong."
      render action: 'edit'
    end
  end

  def destroy
    @product_category.destroy
    flash[:success] = "The Product Category has been Deleted."
    redirect_to admin_product_categories_path
  end

  private 

  def product_category_params
    params.require(:product_category).permit(:name, :permalink, :promoted, :description, :lft, :rgt, :permalink_includes_ancestor, :promotion_id, :parent_id )
  end

  def set_category
    @product_category = ProductCategory.find(params[:id])
  end
end
