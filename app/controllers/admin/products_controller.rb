class Admin::ProductsController < ApplicationController
	before_action :set_product, only: [:update, :edit, :destroy]
  before_action :require_admin_user
  layout 'admin'

	def index
	  @products = Product.ordered.page(params[:page]).per_page(30)
	end

	def new
	  @product = Product.new 
	end

  def edit
  end

	def create
	  @product = Product.new(product_params)
 	  respond_to do |format|
        if @product.save
          @product.stock_level_adjustments.create(description: 'Initial stock')
          if params[:images]
            params[:images].each {|image|
              @product.attachments.create(file: image, parent_type: 'product')
            }
          end
          format.html { 
            flash[:success] = "A new product has been created."
            redirect_to admin_products_path
          }

          format.js
        else
          format.html { 
            flash[:alert] = "Something went wrong."
            render action: 'new'
          }
          format.js
        end
	  end
	end

	def update
	  if @product.update_attributes(product_params)
        if params[:images]
          params[:images].each {|image|
            if !(@product.attachments.find_by_file_name(image.original_filename[0..-5].to_s).nil?)
              @product.attachments.find_by_file_name(image.original_filename[0..-5].to_s).destroy 
            end
            @product.attachments.create(file: image, parent_type: 'product')
          }
        end
        flash[:success] = "The product has been updated."
        redirect_to admin_products_path
      else
        flash[:alert] = "Something went wrong."
      	render action: 'edit'
	  end
	end

  def destroy
    @product.destroy
    flash[:success] = "The Product has been Deleted."
    redirect_to admin_products_path
  end

	private
     
    def product_params
      params.require(:product).permit(:name, :permalink, :description, :active, :weight, :price, :promo_price, :promoted, :promotion_id, :stock_control, :product_category_id)
    end

    def set_product
      @product = Product.find(params[:id])  
    end
end
