class Admin::CountriesController < ApplicationController
	before_action :set_country, only: [:update, :edit, :destroy]
    layout 'admin'

	def index
	  @countries = Country.all.page(params[:page]).per_page(30)
	end

	def new
	  @country = Country.new
	end

	def create
      @country = Country.new(country_params)
      if @country.save
        flash[:success] = "A new country has been created."
        redirect_to admin_countries_path
      else
      	flash[:alert] = "Something went wrong."
        render action: 'new'
      end
	end

    def edit	
    end

	def update
	  if @country.update_attributes(country_params)
        flash[:success] = "The country has been updated."
        redirect_to admin_countries_path
      else
        flash[:alert] = "Something went wrong."
        render action: 'edit'
      end
	end

	def destroy
	  @country.destroy
	  flash[:success] = "The Country has been Deleted."
      redirect_to admin_countries_path
	end

	private 

	def country_params
	   params.require(:country).permit(:name, :currency_id, :flag, :shipment)
	end

	def set_country
	  @country = Country.find(params[:id])
	end
end
