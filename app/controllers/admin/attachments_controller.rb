class Admin::AttachmentsController < ApplicationController
	def create
	  @attachment = Attachment.create(attachment_params)
	end

	private
    
    def attachment_params
       params.require(:attachment).permit(:file, :file_name, :file_type, :file_size, :parent_type, :parent_id)
    end

end
