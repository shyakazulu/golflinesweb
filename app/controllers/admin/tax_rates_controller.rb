class Admin::TaxRatesController < ApplicationController
  before_action :set_tax_rate, only: [:update, :edit, :destroy]
  before_action :require_admin_user
  layout 'admin'

  def index
  	@tax_rates = TaxRate.ordered.page(params[:page]).per_page(30);
  end

  def new
  	@tax_rate = TaxRate.new  
  end

  def edit
  end

  def create
  	@tax_rate = TaxRate.new(tax_rate_params)
  	if @tax_rate.save
      flash[:success] = "A new tax_rate has been created."
      redirect_to admin_tax_rates_path
    else
      flash[:alert] = "Something went wrong."
      render action: 'new'
  	end
  end

  def update
  	if @tax_rate.update_attributes(tax_rate_params)
      flash[:success] = "The tax rate has been updated."
        redirect_to admin_tax_rates_path
      else
        flash[:alert] = "Something went wrong."
      	render action: 'edit'
  	end
  end

  def destroy
  	@tax_rate.destroy
    flash[:success] = "The tax rate has been Deleted."
    redirect_to admin_tax_rates_path
  end

  private
     
    def tax_rate_params
      params.require(:tax_rate).permit(:name, :rate, :address_type, :country_ids => [])
    end

    def set_tax_rate
      @tax_rate = TaxRate.find(params[:id])  
    end
end
