class Admin::OrdersController < ApplicationController
	before_action :set_order, only: [:update, :destroy, :show, :accept, :reject, :ship]
	before_action :require_admin_user, except: [:update]
  before_action :require_user, only: [:update]
	layout 'admin'


	def index
		@orders = Order.ordered.page(params[:page]).per_page(30)
	end
    
    def show
    end

    def new
    	
    end

    def create
    	@order = Order.new(order_params)
	    if @order.save
	      flash[:success] = "A new order has been sent."
	      redirect_to admin_orders_path
	    else
	      flash[:alert] = "Something went wrong."
	      render action: 'new'
	    end
    end

    def update
      if @order.expired?
      	flash[:notice] = "Your order has expired, check out again."
	    redirect_to root_path
      else
      	if @order.update_attributes(order_params)
          @order.order_items.select(&:check_promo)
	 
          flash[:success] = "Please enter your payment details."
	    	  redirect_to purchase_path(token: @order.token)
	    else
          flash[:alert] = "Something went wrong."
          render action: 'edit'
        end
      end
    end

    def accept
    	@order.accept!
    	flash[:success] = "order # #{@order.id} has been accepted!"
    	redirect_to admin_order_path(@order)
    end

    def reject
      @order.reject!
      flash[:success] = "order # #{@order.id} has been rejected!"
      redirect_to admin_order_path(@order)
    end

    def ship
      @order.ship!
      flash[:success] = "order # #{@order.id} has been marked as shipped!"
      redirect_to admin_order_path(@order)
    end

	def destroy
		@order.destroy
        flash[:success] = "The order has been Deleted."
        redirect_to admin_orders_path
	end 

	private

	def set_order
		@order = Order.find(params[:id])
	end

	def order_params
	   params.require(:order).permit(:state, :accepted_at, :received_at, :delivery_service_id, :delivery_price, :rejected_at, :separate_delivery_address, :delivery_name, :delivery_postcode, :amount_paid, :customer_id, :billing_address_id, :shipping_address_id, :token, :promo_code )
	end
end
