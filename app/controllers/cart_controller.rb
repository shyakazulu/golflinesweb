class CartController < ApplicationController
	before_action :require_user
	def add
		id = params[:id]
		size = params[:size]
		quantity  = params[:quantity]
		image_path = params[:image]
		price = params[:price]
		name = params[:name]
		weight = params[:weight]

		if session[:cart] then
			# if the cart has already been created use the the existing one else create a new cart instance.
			cart = session[:cart]
			index = session[:cart].length + 1
        else
        	session[:cart] = {}
        	cart = session[:cart]
        	index = 1;
        end
        
        session[:cart][index] = {"id" => id, "size" => size, "quantity" => quantity, "index" => index, "image_path" => image_path, "price" =>  price, "name" => name, "weight" => weight}

        respond_to do |format|
           format.html {redirect_to :action => :index}
           format.js
        end
	end

	def remove
		index = params[:index]
		session[:cart].delete(index)
		respond_to do |format|
           format.html {redirect_to :action => :index}
           format.js
        end
	end

	def clear
		session[:cart] = nil
		respond_to do |format|
           format.html {redirect_to :action => :index}
           format.js
        end
	end

	def index
		if session[:cart] && session[:user_id] then
          @cart = session[:cart]
		elsif session[:user_id]
          @cart = {}
        else
        	flash[:error] = "Please login to access your cart."
        	redirect_to login_path
		end
		@recommendations = Product.order("RANDOM()").limit(4)
	end
end
