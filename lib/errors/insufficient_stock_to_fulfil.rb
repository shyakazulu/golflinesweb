module Errors
  class InsufficientStockTofulfil < Error
    def order
       @options[:order]
    end

    def out_of_stock_items
    	@options[:out_of_stock_items]
    end
  end
end