require 'rails_helper'

RSpec.describe "promotions/show", type: :view do
  before(:each) do
    @promotion = assign(:promotion, Promotion.create!(
      :rate => "9.99",
      :description => "MyText",
      :visible => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/false/)
  end
end
