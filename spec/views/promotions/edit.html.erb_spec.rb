require 'rails_helper'

RSpec.describe "promotions/edit", type: :view do
  before(:each) do
    @promotion = assign(:promotion, Promotion.create!(
      :rate => "9.99",
      :description => "MyText",
      :visible => false
    ))
  end

  it "renders the edit promotion form" do
    render

    assert_select "form[action=?][method=?]", promotion_path(@promotion), "post" do

      assert_select "input#promotion_rate[name=?]", "promotion[rate]"

      assert_select "textarea#promotion_description[name=?]", "promotion[description]"

      assert_select "input#promotion_visible[name=?]", "promotion[visible]"
    end
  end
end
