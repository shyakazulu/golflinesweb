require 'rails_helper'

RSpec.describe "promotions/index", type: :view do
  before(:each) do
    assign(:promotions, [
      Promotion.create!(
        :rate => "9.99",
        :description => "MyText",
        :visible => false
      ),
      Promotion.create!(
        :rate => "9.99",
        :description => "MyText",
        :visible => false
      )
    ])
  end

  it "renders a list of promotions" do
    render
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
