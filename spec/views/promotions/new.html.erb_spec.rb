require 'rails_helper'

RSpec.describe "promotions/new", type: :view do
  before(:each) do
    assign(:promotion, Promotion.new(
      :rate => "9.99",
      :description => "MyText",
      :visible => false
    ))
  end

  it "renders new promotion form" do
    render

    assert_select "form[action=?][method=?]", promotions_path, "post" do

      assert_select "input#promotion_rate[name=?]", "promotion[rate]"

      assert_select "textarea#promotion_description[name=?]", "promotion[description]"

      assert_select "input#promotion_visible[name=?]", "promotion[visible]"
    end
  end
end
