require 'rails_helper'

RSpec.describe "products/edit", type: :view do
  before(:each) do
    @product = assign(:product, Product.create!(
      :name => "MyString",
      :premalink => "MyString",
      :description => "MyText",
      :active => false,
      :weight => "9.99",
      :price => "9.99",
      :promo_price => "9.99",
      :promoted => false,
      :promotion_id => 1
    ))
  end

  it "renders the edit product form" do
    render

    assert_select "form[action=?][method=?]", product_path(@product), "post" do

      assert_select "input#product_name[name=?]", "product[name]"

      assert_select "input#product_premalink[name=?]", "product[premalink]"

      assert_select "textarea#product_description[name=?]", "product[description]"

      assert_select "input#product_active[name=?]", "product[active]"

      assert_select "input#product_weight[name=?]", "product[weight]"

      assert_select "input#product_price[name=?]", "product[price]"

      assert_select "input#product_promo_price[name=?]", "product[promo_price]"

      assert_select "input#product_promoted[name=?]", "product[promoted]"

      assert_select "input#product_promotion_id[name=?]", "product[promotion_id]"
    end
  end
end
