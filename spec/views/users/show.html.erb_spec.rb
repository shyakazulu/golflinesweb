require 'rails_helper'

RSpec.describe "users/show", type: :view do
  before(:each) do
    @user = assign(:user, User.create!(
      :first_name => "First Name",
      :last_name => "Last Name",
      :email => "Email",
      :password_digest => "Password Digest",
      :password_reset_token => "Password Reset Token",
      :password_resent_token_sent_at => "Password Resent Token Sent At",
      :avatar => "Avatar",
      :phone => "Phone"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/First Name/)
    expect(rendered).to match(/Last Name/)
    expect(rendered).to match(/Email/)
    expect(rendered).to match(/Password Digest/)
    expect(rendered).to match(/Password Reset Token/)
    expect(rendered).to match(/Password Resent Token Sent At/)
    expect(rendered).to match(/Avatar/)
    expect(rendered).to match(/Phone/)
  end
end
