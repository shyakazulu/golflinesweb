require 'rails_helper'

RSpec.describe "users/edit", type: :view do
  before(:each) do
    @user = assign(:user, User.create!(
      :first_name => "MyString",
      :last_name => "MyString",
      :email => "MyString",
      :password_digest => "MyString",
      :password_reset_token => "MyString",
      :password_resent_token_sent_at => "MyString",
      :avatar => "MyString",
      :phone => "MyString"
    ))
  end

  it "renders the edit user form" do
    render

    assert_select "form[action=?][method=?]", user_path(@user), "post" do

      assert_select "input#user_first_name[name=?]", "user[first_name]"

      assert_select "input#user_last_name[name=?]", "user[last_name]"

      assert_select "input#user_email[name=?]", "user[email]"

      assert_select "input#user_password_digest[name=?]", "user[password_digest]"

      assert_select "input#user_password_reset_token[name=?]", "user[password_reset_token]"

      assert_select "input#user_password_resent_token_sent_at[name=?]", "user[password_resent_token_sent_at]"

      assert_select "input#user_avatar[name=?]", "user[avatar]"

      assert_select "input#user_phone[name=?]", "user[phone]"
    end
  end
end
