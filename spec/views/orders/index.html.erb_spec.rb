require 'rails_helper'

RSpec.describe "orders/index", type: :view do
  before(:each) do
    assign(:orders, [
      Order.create!(
        :first_name => "First Name",
        :last_name => "Last Name",
        :billing_address => "Billing Address",
        :country_id => 1,
        :email_address => "Email Address",
        :phone_number => "Phone Number",
        :state => "State",
        :delivery_service_id => 2,
        :delivery_price => "9.99",
        :separate_delivery_address => "Separate Delivery Address",
        :boolean => "Boolean",
        :delivery_address => "Delivery Address",
        :total_price => "9.99",
        :user_id => 3
      ),
      Order.create!(
        :first_name => "First Name",
        :last_name => "Last Name",
        :billing_address => "Billing Address",
        :country_id => 1,
        :email_address => "Email Address",
        :phone_number => "Phone Number",
        :state => "State",
        :delivery_service_id => 2,
        :delivery_price => "9.99",
        :separate_delivery_address => "Separate Delivery Address",
        :boolean => "Boolean",
        :delivery_address => "Delivery Address",
        :total_price => "9.99",
        :user_id => 3
      )
    ])
  end

  it "renders a list of orders" do
    render
    assert_select "tr>td", :text => "First Name".to_s, :count => 2
    assert_select "tr>td", :text => "Last Name".to_s, :count => 2
    assert_select "tr>td", :text => "Billing Address".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Email Address".to_s, :count => 2
    assert_select "tr>td", :text => "Phone Number".to_s, :count => 2
    assert_select "tr>td", :text => "State".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "Separate Delivery Address".to_s, :count => 2
    assert_select "tr>td", :text => "Boolean".to_s, :count => 2
    assert_select "tr>td", :text => "Delivery Address".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
  end
end
