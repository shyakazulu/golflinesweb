require 'rails_helper'

RSpec.describe "orders/edit", type: :view do
  before(:each) do
    @order = assign(:order, Order.create!(
      :first_name => "MyString",
      :last_name => "MyString",
      :billing_address => "MyString",
      :country_id => 1,
      :email_address => "MyString",
      :phone_number => "MyString",
      :state => "MyString",
      :delivery_service_id => 1,
      :delivery_price => "9.99",
      :separate_delivery_address => "MyString",
      :boolean => "MyString",
      :delivery_address => "MyString",
      :total_price => "9.99",
      :user_id => 1
    ))
  end

  it "renders the edit order form" do
    render

    assert_select "form[action=?][method=?]", order_path(@order), "post" do

      assert_select "input#order_first_name[name=?]", "order[first_name]"

      assert_select "input#order_last_name[name=?]", "order[last_name]"

      assert_select "input#order_billing_address[name=?]", "order[billing_address]"

      assert_select "input#order_country_id[name=?]", "order[country_id]"

      assert_select "input#order_email_address[name=?]", "order[email_address]"

      assert_select "input#order_phone_number[name=?]", "order[phone_number]"

      assert_select "input#order_state[name=?]", "order[state]"

      assert_select "input#order_delivery_service_id[name=?]", "order[delivery_service_id]"

      assert_select "input#order_delivery_price[name=?]", "order[delivery_price]"

      assert_select "input#order_separate_delivery_address[name=?]", "order[separate_delivery_address]"

      assert_select "input#order_boolean[name=?]", "order[boolean]"

      assert_select "input#order_delivery_address[name=?]", "order[delivery_address]"

      assert_select "input#order_total_price[name=?]", "order[total_price]"

      assert_select "input#order_user_id[name=?]", "order[user_id]"
    end
  end
end
