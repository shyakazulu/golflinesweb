require 'rails_helper'

RSpec.describe "orders/show", type: :view do
  before(:each) do
    @order = assign(:order, Order.create!(
      :first_name => "First Name",
      :last_name => "Last Name",
      :billing_address => "Billing Address",
      :country_id => 1,
      :email_address => "Email Address",
      :phone_number => "Phone Number",
      :state => "State",
      :delivery_service_id => 2,
      :delivery_price => "9.99",
      :separate_delivery_address => "Separate Delivery Address",
      :boolean => "Boolean",
      :delivery_address => "Delivery Address",
      :total_price => "9.99",
      :user_id => 3
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/First Name/)
    expect(rendered).to match(/Last Name/)
    expect(rendered).to match(/Billing Address/)
    expect(rendered).to match(/1/)
    expect(rendered).to match(/Email Address/)
    expect(rendered).to match(/Phone Number/)
    expect(rendered).to match(/State/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/Separate Delivery Address/)
    expect(rendered).to match(/Boolean/)
    expect(rendered).to match(/Delivery Address/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/3/)
  end
end
