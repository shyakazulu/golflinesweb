

FactoryGirl.define do 
  factory :user, :aliasses => [:customer, :admin] do
    sequence(:email) {|n| "foo#{n}@example.com"}
    password_digest "secret"
    first_name "John"
	last_name "Doe"
	phone "+86 1234 5555 655"
  end

  factory :promotion do
    rate 30
    description "30% off all Hoodies"
    visible true
  end

  factory :product do
    name "camo-gray"
    permalink "/camo-gray"
    description "Lorem ipsum dolor sit amet, consectetur adipisicing elit
                 A consequuntur, fugiat earum! Ratione nulla hic eaque totam
                 id, alias deserunt quam ut. Voluptas aspernatur omnis nobis"
    active true
    weight 1.00
    price  30.00
    promo_price 20.00
    promoted true
    sequence(:promotion_id) { |n| n}
  end

  factory :product_tranlsation do
    sequence(:product_id) {|n| n}
    name "camo-gray"
    permalink "/camo-gray"
    description "Lorem ipsum dolor sit amet, consectetur adipisicing elit
                 A consequuntur, fugiat earum! Ratione nulla hic eaque totam
                 id, alias deserunt quam ut. Voluptas aspernatur omnis nobis"

  end

  factory :product_category_transations do
    locale "en"
    permalink "/en"
    name "tees"
  end

  factory :payment do
    sequence(:order_id) {|n| n}
    amount 30.00
    method "paypal"
    confirmed "true"
    refundable "true"
    amount_refunded 20.00
    parent_payment_id 20
    reference "reference"
  end
end