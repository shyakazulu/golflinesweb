# Preview all emails at http://localhost:3000/rails/mailers/order_mailer
class OrderMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/order_mailer/recieved
  def recieved
    OrderMailer.recieved
  end

  # Preview this email at http://localhost:3000/rails/mailers/order_mailer/accepted
  def accepted
    OrderMailer.accepted
  end

  # Preview this email at http://localhost:3000/rails/mailers/order_mailer/rejected
  def rejected
    OrderMailer.rejected
  end

  # Preview this email at http://localhost:3000/rails/mailers/order_mailer/shipped
  def shipped
    OrderMailer.shipped
  end

end
