require "rails_helper"

RSpec.describe OrderMailer, type: :mailer do
  describe "recieved" do
    let(:mail) { OrderMailer.recieved }

    it "renders the headers" do
      expect(mail.subject).to eq("Recieved")
      expect(mail.to).to eq(["to@example.org"])
      expect(mail.from).to eq(["from@example.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("Hi")
    end
  end

  describe "accepted" do
    let(:mail) { OrderMailer.accepted }

    it "renders the headers" do
      expect(mail.subject).to eq("Accepted")
      expect(mail.to).to eq(["to@example.org"])
      expect(mail.from).to eq(["from@example.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("Hi")
    end
  end

  describe "rejected" do
    let(:mail) { OrderMailer.rejected }

    it "renders the headers" do
      expect(mail.subject).to eq("Rejected")
      expect(mail.to).to eq(["to@example.org"])
      expect(mail.from).to eq(["from@example.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("Hi")
    end
  end

  describe "shipped" do
    let(:mail) { OrderMailer.shipped }

    it "renders the headers" do
      expect(mail.subject).to eq("Shipped")
      expect(mail.to).to eq(["to@example.org"])
      expect(mail.from).to eq(["from@example.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("Hi")
    end
  end

end
