require 'rails_helper'

RSpec.describe PublicController, type: :controller do

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #shop" do
    it "returns http success" do
      get :shop
      expect(response).to have_http_status(:success)
    end
  end

end
