# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160412185650) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.integer  "customer_id"
    t.string   "address_type"
    t.boolean  "default"
    t.string   "address1"
    t.string   "address2"
    t.string   "address3"
    t.string   "address4"
    t.string   "post_code"
    t.integer  "country_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "phone_number"
    t.string   "email_address"
    t.integer  "order_id"
  end

  create_table "article_translations", force: :cascade do |t|
    t.integer  "article_id", null: false
    t.string   "locale",     null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "title"
    t.text     "content"
  end

  add_index "article_translations", ["article_id"], name: "index_article_translations_on_article_id", using: :btree
  add_index "article_translations", ["locale"], name: "index_article_translations_on_locale", using: :btree

  create_table "articles", force: :cascade do |t|
    t.string   "title"
    t.text     "content"
    t.integer  "admin_id"
    t.datetime "pub_date",   default: '2016-04-26 15:36:47'
    t.boolean  "visible",    default: true
    t.text     "credits"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.string   "permalink"
  end

  create_table "attachments", force: :cascade do |t|
    t.integer  "parent_id"
    t.string   "parent_type"
    t.string   "token"
    t.string   "file",        null: false
    t.string   "file_name"
    t.integer  "file_size"
    t.string   "file_type"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "comments", force: :cascade do |t|
    t.integer  "customer_id"
    t.text     "content"
    t.integer  "likes"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "parent_id"
    t.integer  "article_id"
  end

  create_table "countries", force: :cascade do |t|
    t.string   "name"
    t.integer  "currency_id"
    t.string   "flag"
    t.boolean  "shipment"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "currencies", force: :cascade do |t|
    t.string   "name"
    t.decimal  "rate",       precision: 8, scale: 2
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.string   "locale",                             default: "en"
  end

  create_table "delivery_service_prices", force: :cascade do |t|
    t.integer  "delivery_service_id"
    t.string   "code"
    t.decimal  "price",               precision: 8, scale: 2
    t.decimal  "cost_price",          precision: 8, scale: 2
    t.integer  "tax_rate_id"
    t.decimal  "min_weight",          precision: 8, scale: 2
    t.decimal  "max_weight",          precision: 8, scale: 2
    t.text     "country_ids"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
  end

  add_index "delivery_service_prices", ["delivery_service_id"], name: "index_delivery_service_prices_on_delivery_service_id", using: :btree
  add_index "delivery_service_prices", ["max_weight"], name: "index_delivery_service_prices_on_max_weight", using: :btree
  add_index "delivery_service_prices", ["min_weight"], name: "index_delivery_service_prices_on_min_weight", using: :btree
  add_index "delivery_service_prices", ["price"], name: "index_delivery_service_prices_on_price", using: :btree

  create_table "delivery_services", force: :cascade do |t|
    t.string   "name"
    t.boolean  "default",      default: false
    t.boolean  "active",       default: true
    t.string   "tracking_url"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "courier"
    t.string   "code"
  end

  add_index "delivery_services", ["active"], name: "index_delivery_services_on_active", using: :btree

  create_table "order_items", force: :cascade do |t|
    t.integer  "order_id"
    t.integer  "ordered_item_id"
    t.string   "ordered_item_type"
    t.integer  "quantity"
    t.decimal  "unit_price",        precision: 8, scale: 2
    t.decimal  "weight",            precision: 8, scale: 2
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.string   "size"
  end

  add_index "order_items", ["order_id"], name: "index_order_items_on_order_id", using: :btree
  add_index "order_items", ["ordered_item_id", "ordered_item_type"], name: "index_order_items_ordered_item", using: :btree

  create_table "orders", force: :cascade do |t|
    t.string   "state"
    t.datetime "accepted_at"
    t.datetime "received_at"
    t.integer  "delivery_service_id"
    t.decimal  "delivery_price",            precision: 8, scale: 2
    t.datetime "rejected_at"
    t.boolean  "separate_delivery_address"
    t.decimal  "amount_paid",               precision: 8, scale: 2
    t.integer  "customer_id"
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.integer  "billing_address_id"
    t.integer  "shipping_address_id"
    t.string   "token"
    t.datetime "shipped_at"
    t.string   "promo_code"
  end

  add_index "orders", ["delivery_service_id"], name: "index_orders_on_delivery_service_id", using: :btree
  add_index "orders", ["received_at"], name: "index_orders_on_received_at", using: :btree

  create_table "payments", force: :cascade do |t|
    t.integer  "order_id"
    t.decimal  "amount",            precision: 8, scale: 2
    t.string   "method"
    t.boolean  "confirmed"
    t.boolean  "refundable"
    t.decimal  "amount_refunded",   precision: 8, scale: 2
    t.integer  "parent_payment_id"
    t.string   "reference"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "payments", ["order_id"], name: "index_payments_on_order_id", using: :btree
  add_index "payments", ["parent_payment_id"], name: "index_payments_on_parent_payment_id", using: :btree

  create_table "product_categories", force: :cascade do |t|
    t.string   "name"
    t.string   "permalink"
    t.text     "description"
    t.integer  "parent_id"
    t.integer  "lft"
    t.integer  "rgt"
    t.integer  "depth"
    t.string   "ancestral_permalink"
    t.boolean  "permalink_includes_ancestor", default: false
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.integer  "promotion_id"
    t.boolean  "promoted",                    default: false
  end

  add_index "product_categories", ["permalink"], name: "index_product_categories_on_permalink", using: :btree
  add_index "product_categories", ["promotion_id"], name: "index_product_categories_on_promotion_id", using: :btree

  create_table "product_categorizations", force: :cascade do |t|
    t.integer  "product_id"
    t.integer  "product_category_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "product_category_translations", force: :cascade do |t|
    t.integer  "product_category_id", null: false
    t.string   "locale",              null: false
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "name"
    t.text     "description"
  end

  add_index "product_category_translations", ["locale"], name: "index_product_category_translations_on_locale", using: :btree
  add_index "product_category_translations", ["product_category_id"], name: "index_product_category_translations_on_product_category_id", using: :btree

  create_table "product_tranlsations", force: :cascade do |t|
    t.integer  "product_id"
    t.string   "name"
    t.string   "permalink"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "product_translations", force: :cascade do |t|
    t.integer  "product_id",  null: false
    t.string   "locale",      null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "name"
    t.text     "description"
  end

  add_index "product_translations", ["locale"], name: "index_product_translations_on_locale", using: :btree
  add_index "product_translations", ["product_id"], name: "index_product_translations_on_product_id", using: :btree

  create_table "products", force: :cascade do |t|
    t.string   "name"
    t.string   "permalink"
    t.text     "description"
    t.boolean  "active",                                      default: true
    t.decimal  "weight",              precision: 8, scale: 2
    t.decimal  "price",               precision: 8, scale: 2
    t.decimal  "promo_price",         precision: 8, scale: 2
    t.boolean  "promoted",                                    default: false
    t.integer  "promotion_id"
    t.datetime "created_at",                                                  null: false
    t.datetime "updated_at",                                                  null: false
    t.boolean  "stock_control"
    t.integer  "product_category_id"
    t.integer  "parent_id"
  end

  add_index "products", ["permalink"], name: "index_products_on_permalink", using: :btree
  add_index "products", ["product_category_id"], name: "index_products_on_product_category_id", using: :btree
  add_index "products", ["promotion_id"], name: "index_products_on_promotion_id", using: :btree

  create_table "promotion_translations", force: :cascade do |t|
    t.integer  "promotion_id", null: false
    t.string   "locale",       null: false
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.text     "description"
  end

  add_index "promotion_translations", ["locale"], name: "index_promotion_translations_on_locale", using: :btree
  add_index "promotion_translations", ["promotion_id"], name: "index_promotion_translations_on_promotion_id", using: :btree

  create_table "promotions", force: :cascade do |t|
    t.text     "description"
    t.boolean  "visible"
    t.datetime "created_at",                                                          null: false
    t.datetime "updated_at",                                                          null: false
    t.decimal  "rate",        precision: 8, scale: 2
    t.string   "code"
    t.datetime "valid_until",                         default: '2016-04-26 15:36:58'
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "rs_evaluations", force: :cascade do |t|
    t.string   "reputation_name"
    t.integer  "source_id"
    t.string   "source_type"
    t.integer  "target_id"
    t.string   "target_type"
    t.float    "value",           default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "data"
  end

  add_index "rs_evaluations", ["reputation_name", "source_id", "source_type", "target_id", "target_type"], name: "index_rs_evaluations_on_reputation_name_and_source_and_target", unique: true, using: :btree
  add_index "rs_evaluations", ["reputation_name"], name: "index_rs_evaluations_on_reputation_name", using: :btree
  add_index "rs_evaluations", ["source_id", "source_type"], name: "index_rs_evaluations_on_source_id_and_source_type", using: :btree
  add_index "rs_evaluations", ["target_id", "target_type"], name: "index_rs_evaluations_on_target_id_and_target_type", using: :btree

  create_table "rs_reputation_messages", force: :cascade do |t|
    t.integer  "sender_id"
    t.string   "sender_type"
    t.integer  "receiver_id"
    t.float    "weight",      default: 1.0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rs_reputation_messages", ["receiver_id", "sender_id", "sender_type"], name: "index_rs_reputation_messages_on_receiver_id_and_sender", unique: true, using: :btree
  add_index "rs_reputation_messages", ["receiver_id"], name: "index_rs_reputation_messages_on_receiver_id", using: :btree
  add_index "rs_reputation_messages", ["sender_id", "sender_type"], name: "index_rs_reputation_messages_on_sender_id_and_sender_type", using: :btree

  create_table "rs_reputations", force: :cascade do |t|
    t.string   "reputation_name"
    t.float    "value",           default: 0.0
    t.string   "aggregated_by"
    t.integer  "target_id"
    t.string   "target_type"
    t.boolean  "active",          default: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "data"
  end

  add_index "rs_reputations", ["reputation_name", "target_id", "target_type"], name: "index_rs_reputations_on_reputation_name_and_target", unique: true, using: :btree
  add_index "rs_reputations", ["reputation_name"], name: "index_rs_reputations_on_reputation_name", using: :btree
  add_index "rs_reputations", ["target_id", "target_type"], name: "index_rs_reputations_on_target_id_and_target_type", using: :btree

  create_table "stock_level_adjustments", force: :cascade do |t|
    t.text     "description"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "adjustment_s",   default: 0
    t.integer  "adjustment_xs",  default: 0
    t.integer  "adjustment_m",   default: 0
    t.integer  "adjustment_l",   default: 0
    t.integer  "adjustment_xl",  default: 0
    t.integer  "adjustment_xxl", default: 0
    t.integer  "product_id"
  end

  add_index "stock_level_adjustments", ["product_id"], name: "index_stock_level_adjustments_on_product_id", using: :btree

  create_table "tax_rates", force: :cascade do |t|
    t.string   "name"
    t.decimal  "rate",         precision: 8, scale: 2
    t.text     "country_ids"
    t.string   "address_type"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "password_digest"
    t.string   "password_reset_token"
    t.string   "password_resent_token_sent_at"
    t.string   "phone"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "job_title"
    t.string   "facebook"
    t.string   "instagram"
    t.string   "twitter"
    t.integer  "braintree_customer_id"
  end

  add_index "users", ["email"], name: "index_users_on_email", using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

end
