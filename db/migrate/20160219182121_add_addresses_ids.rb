class AddAddressesIds < ActiveRecord::Migration
  def change
  	add_column :orders, :billing_address_id, :integer, index: true
  	add_column :orders, :shipping_address_id, :integer, index: true
  	remove_column :orders, :billing_address1, :integer
  	remove_column :orders, :billing_address2, :integer
  	remove_column :orders, :billing_address3, :integer
  	remove_column :orders, :billing_address4, :integer
  end
end
