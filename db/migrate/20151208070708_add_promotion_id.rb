class AddPromotionId < ActiveRecord::Migration
  def change
  	add_column :product_categories, :promotion_id, :integer
  	add_column :product_categories, :promoted, :boolean, default: false
  end
end
