class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :password_digest
      t.string :password_reset_token
      t.string :password_resent_token_sent_at
      t.string :avatar
      t.string :phone

      t.timestamps null: false
    end
  end
end
