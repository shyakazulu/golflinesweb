class RefactorOrders < ActiveRecord::Migration
  def change
  	remove_column :orders, :first_name, :string
  	remove_column :orders, :last_name, :string
  	remove_column :orders, :country_id, :integer
  	remove_column :orders, :phone_number, :string
  	remove_column :orders, :email_address, :string
  	remove_column :orders, :delivery_country_id, :integer
  end
end
