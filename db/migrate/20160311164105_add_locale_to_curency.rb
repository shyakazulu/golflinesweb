class AddLocaleToCurency < ActiveRecord::Migration
  def change
  	add_column :currencies, :locale, :string, default: "en"
  end
end
