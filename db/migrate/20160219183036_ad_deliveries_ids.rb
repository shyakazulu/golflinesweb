class AdDeliveriesIds < ActiveRecord::Migration
  def change
  	remove_column :orders, :delivery_address1, :string
  	remove_column :orders, :delivery_address2, :string
  	remove_column :orders, :delivery_address3, :string
  	remove_column :orders, :delivery_address4, :string
  end
end
