class CreateProductTranlsations < ActiveRecord::Migration
  def change
    create_table :product_tranlsations do |t|
      t.integer :product_id
      t.string :name
      t.string :permalink
      t.text :description

      t.timestamps null: false
    end
  end
end
