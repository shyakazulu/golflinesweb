class CreateCountries < ActiveRecord::Migration
  def change
    create_table :countries do |t|
      t.string :name
      t.integer :currency_id
      t.string :flag
      t.boolean :shipment

      t.timestamps null: false
    end
  end
end
