class AddIndexesToProducts < ActiveRecord::Migration
  def change
  	remove_column :promotions, :rate, :decimal
  	add_column :promotions, :rate, :decimal, precision: 8, scale: 2
  	add_index :products, :promotion_id
  	add_index :product_categories, :promotion_id
  end
end
