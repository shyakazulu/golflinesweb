class CreatePromotions < ActiveRecord::Migration
  def change
    create_table :promotions do |t|
      t.decimal :rate
      t.text :description ,   precision: 8, scale: 2
      t.boolean :visible

      t.timestamps null: false
    end
  end
end
