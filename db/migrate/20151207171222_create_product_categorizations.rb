class CreateProductCategorizations < ActiveRecord::Migration
  def change
    create_table :product_categorizations do |t|
      t.integer :product_id
      t.integer :product_category_id

      t.timestamps null: false
    end
  end
end
