class RemovePostcodeFromOrder < ActiveRecord::Migration
  def change
  	remove_column :orders, :delivery_name, :string
  	remove_column :orders, :delivery_postcode, :string
  end
end
