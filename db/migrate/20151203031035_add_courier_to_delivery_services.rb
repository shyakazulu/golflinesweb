class AddCourierToDeliveryServices < ActiveRecord::Migration
  def change
    add_column :delivery_services, :courier, :string
    add_index :orders, :delivery_service_id
    add_index :orders, :received_at
    add_index :users, :email
  end
end
