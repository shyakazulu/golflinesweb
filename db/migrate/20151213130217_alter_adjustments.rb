class AlterAdjustments < ActiveRecord::Migration
  def change
  	remove_column :stock_level_adjustments, :adjustment, :integer
  	remove_column :stock_level_adjustments, :parent_type, :string
  	remove_column :stock_level_adjustments, :parent_id, :integer
  	add_column :stock_level_adjustments, :adjustment_s, :integer, default: 0
  	add_column :stock_level_adjustments, :adjustment_xs, :integer, default: 0
  	add_column :stock_level_adjustments, :adjustment_m, :integer, default: 0
  	add_column :stock_level_adjustments, :adjustment_l, :integer, default: 0
  	add_column :stock_level_adjustments, :adjustment_xl, :integer, default: 0
  	add_column :stock_level_adjustments, :adjustment_xxl, :integer, default: 0
  end
end
