class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :first_name
      t.string :last_name
      t.string :billing_address1
      t.string :billing_address2
      t.string :billing_address3
      t.string :billing_address4
      t.integer :country_id
      t.string :email_address
      t.string :phone_number
      t.string :state
      t.datetime :accepted_at
      t.datetime :received_at
      t.integer :delivery_service_id
      t.decimal :delivery_price,          precision: 8, scale: 2
      t.datetime :rejected_at
      t.boolean :separate_delivery_address
      t.string :delivery_name
      t.string :delivery_address1
      t.string :delivery_address2
      t.string :delivery_address3
      t.string :delivery_address4
      t.string :delivery_postcode
      t.integer :delivery_country_id
      t.decimal :amount_paid,             precision: 8, scale: 2
      t.integer :customer_id

      t.timestamps null: false
    end
  end
end
