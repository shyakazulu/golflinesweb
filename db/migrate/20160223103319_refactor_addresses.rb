class RefactorAddresses < ActiveRecord::Migration
  def change
  	add_column :addresses, :first_name, :string
  	add_column :addresses, :last_name, :string
  	add_column :addresses, :phone_number, :string
    add_column :addresses, :email_address, :string
  end
end
