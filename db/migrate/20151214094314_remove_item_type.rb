class RemoveItemType < ActiveRecord::Migration
  def change
  	remove_column :stock_level_adjustments, :item_type, :string
  end
end
