class AddParentToComment < ActiveRecord::Migration
  def change
  	add_column :comments, :parent_id, :integer
  	add_column :comments, :article_id, :integer
  end
end
