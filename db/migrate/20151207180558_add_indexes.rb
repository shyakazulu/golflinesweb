class AddIndexes < ActiveRecord::Migration
  def change
    add_index :order_items, [:ordered_item_id, :ordered_item_type], name: 'index_order_items_ordered_item'
    add_index :order_items, :order_id
    add_index :payments, :order_id
    add_index :payments, :parent_payment_id
    add_index :product_categories, :permalink
    add_column :products, :product_category_id, :integer
    add_index :products, :product_category_id
    add_index :products, :permalink
    add_index :delivery_services, :active
    add_index :delivery_service_prices, :delivery_service_id
    add_index :delivery_service_prices, :min_weight
    add_index :delivery_service_prices, :max_weight
    add_index :delivery_service_prices, :price
  end
end

