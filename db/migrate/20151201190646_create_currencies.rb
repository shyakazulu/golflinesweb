class CreateCurrencies < ActiveRecord::Migration
  def change
    create_table :currencies do |t|
      t.string :name
      t.decimal :rate,  precision: 8, scale: 2

      t.timestamps null: false
    end
  end
end
