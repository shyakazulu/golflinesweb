class AddIndexToSla < ActiveRecord::Migration
  def change
  	remove_column :stock_level_adjustments, :item_id, :integer
  	add_index :stock_level_adjustments, :product_id
  end
end
