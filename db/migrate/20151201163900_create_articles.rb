class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title
      t.text :content
      t.integer :admin_id
      t.integer :likes,        default: 0
      t.datetime :pub_date,    default: DateTime.now
      t.boolean :visible,      default: true
      t.text :credits

      t.timestamps null: false
    end
  end
end
