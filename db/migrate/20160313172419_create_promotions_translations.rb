class CreatePromotionsTranslations < ActiveRecord::Migration
  def self.up
    Promotion.create_translation_table!({
    	:description => :text
	}, {
	   :migrate_data => true
	})
  end

  def self.down
  	Promotion.drop_translation_table! :migrate_data => true
  end
end
