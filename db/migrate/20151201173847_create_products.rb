class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.string :permalink
      t.text :description
      t.boolean :active,      default: true
      t.decimal :weight,      precision: 8, scale: 2
      t.decimal :price,       precision: 8, scale: 2
      t.decimal :promo_price, precision: 8, scale: 2
      t.boolean :promoted,    default: false
      t.integer :promotion_id

      t.timestamps null: false
    end
  end
end
