class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.integer :order_id
      t.decimal :amount,             precision: 8, scale: 2
      t.string :method
      t.boolean :confirmed
      t.boolean :refundable
      t.decimal :amount_refunded,    precision: 8, scale: 2
      t.integer :parent_payment_id
      t.string :reference

      t.timestamps null: false
    end
  end
end
