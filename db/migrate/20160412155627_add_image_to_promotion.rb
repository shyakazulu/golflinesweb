class AddImageToPromotion < ActiveRecord::Migration
  def change
  	add_column :promotions, :valid_until, :datetime, default: DateTime.now
  end
end
