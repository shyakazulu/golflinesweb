class AddJobTitteToUser < ActiveRecord::Migration
  def change
  	remove_column :users, :avatar, :string
  	add_column :users, :job_title, :string
  end
end
