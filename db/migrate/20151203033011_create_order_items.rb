class CreateOrderItems < ActiveRecord::Migration
  def change
    create_table :order_items do |t|
      t.integer :order_id
      t.integer :ordered_item_id
      t.string :ordered_item_type
      t.integer :quantity
      t.decimal :unit_price,    precision: 8, scale: 2
      t.decimal :weight,        precision: 8, scale: 2
      t.timestamps null: false
    end
  end
end
