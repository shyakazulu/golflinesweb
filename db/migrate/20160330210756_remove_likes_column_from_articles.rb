class RemoveLikesColumnFromArticles < ActiveRecord::Migration
  def change
  	remove_column :articles, :likes, :integer
  end
end
